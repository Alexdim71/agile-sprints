The main goal of the project is to implement a web application allowing the Users (Developers, Product Owners, and Administrators)
to define and manage new Projects, Sprints and Tasks, as well as to report their progress.
More information about Agile Software Development can be found here: https://en.wikipedia.org/wiki/Agile_software_development.
All Users should have following common attributes:
•	id (generated automatically) - long number;
•	firstName - string 2 to 15 characters long;
•	lastName - string 2 to 15 characters long;
•	email - should be valid email address, unique within the system, cannot be changed;
•	username - string 2 to 15 characters long - word characters only, unique within the system, cannot be changed;
•	password - string 8 to 15 characters long, at least one digit, one capital letter, and one sign different than letter or digit, NOT sent back to the User clients for security reasons;
•	roles - DEVELOPER, PRODUCT_OWNER or ADMIN Enumerated Set (more than one role can be assigned to the User), created and editable only by Administrators;
•	contacts - string 10 - 250 characters long;
•	status - validity status of the user account (ACTIVE, CHANGE_PASSWORD - default, users should change password on first login, SUSPENDED, or DEACTIVATED), visible and editable only by Administrators;
•	created (generated automatically) - time stamp of the moment the User account was created;
•	modified (generated automatically) - time stamp of the moment the User account was last modified;
The Administrators do not have additional fields. The Developers should have in addition to the User's attributes, the following fields::
•	assignedTasks - list of all tasks currently assigned;
•	completedTaskResults - list of TaskResults for the tasks already completed by the Developer;
The ProductOwner should have in addition to the User's attributes, the following fields:
•	projects - list of currently assigned projects the ProductOwner is responsible for;
•	completedProjectResults - list of ProjectResults for the projects already completed by the ProductOwner;
The Developers and ProductOwners should be able to manage their own personal data and change their passwords. The Administrators should be able to manage all User's data, except their passwords. The IDs of all entities should be generated automatically and should NOT be changeable.
The Administrators should be able to add new Projects, as well as to complete them after the ProjectResult has been reported by ProductOwner. The ProductOwners and Developers should be able to add new Sprints and Tasks to existing Projects, as well as to verify/report completion of TaskResults and SprintResults for different Tasks and Sprints completed by Developers.
Each Project has the following structure:
•	id (generated automatically) - long number;
•	title - string 2 to 120 characters long;
•	startDate - the start date of the Project;
•	description (optional) - string 10 - 2500 characters long, supporting Markdown syntax;
•	owner - the ProductOwner responsible for the project;
•	developers - developers implementing the Project;
•	currentSprint - the Sprint entity representing the currently active Sprint;
•	previousSprintResults - the completion results for previous Sprints in the Project, including TaskResults;
•	tasksBacklog - list of Tasks planned for the project, new Tasks can be added/removed dynamically during the project;
•	tags - string including comma separated tags, allowing to find the Publication by quick search;
•	created (generated automatically) - time stamp of the moment the entity was created;
•	modified (generated automatically) - time stamp of the moment the entity was last modified;
Each Sprint has the following structure:
•	id (generated automatically) - long number;
•	startDate - start date for the sprint;
•	endDate - end date for the sprint;
•	duration - integer, number of working days;
•	owner - the ProductOwner responsible for the Sprint;
•	developers - developers participating in the Sprint;
•	tasks - list of Tasks assigned to the current Sprint;
•	completedTaskResults - list of TaskResults for the Tasks already completed in the Sprint;
•	created (generated automatically) - time stamp of the moment the entity was created;
•	modified (generated automatically) - time stamp of the moment the entity was last modified;
Each Task has the following structure:
•	id (generated automatically) - long number;
•	kind - enumeration: RESEARCH, DESIGN, PROTOTYPING, IMPLEMENTATION, QA, OPERATIONS, BUG_FIXING, DOCUMENTATION, OTHER;
•	title - string 2 to 120 characters long;
•	addedBy - the User that has added the Task;
•	estimatedEffort - integer number in effort units
(the same units in which the team velocity is estimated - http://wiki.c2.com/?IdealProgrammingTime) ;
•	status - enumeration PLANNED, ACTIVE, COMPLETED;
•	sprint (optional) - the Sprint the Task belongs, if already assigned (status ACTIVE or COMPLETED);
•	developersAssigned - list of Developers assigned to the Task;
•	description (optional) - string 150 - 2500 characters long, supporting Markdown syntax;
•	tags - string including comma separated tags, allowing to find the Task by quick search;
•	created (generated automatically) - time stamp of the moment the entity was created;
•	modified (generated automatically) - time stamp of the moment the entity was last modified;
Each ProjectResult has the following structure:
•	id (generated automatically) - long number;
•	project - the Project this result is reported for;
•	endDate - end date for the project;
•	duration - integer, number of working days for the project;
•	resultsDescription (optional) - string 10 - 2500 characters long, supporting Markdown syntax;
•	sprintResults - list of SprintResult for the Sprints completed;
•	created (generated automatically) - time stamp of the moment the entity was created;
•	modified (generated automatically) - time stamp of the moment the entity was last modified;
Each SprintResult has the following structure:
•	id (generated automatically) - long number;
•	sprint - the Sprint this result is reported for;
•	teamVelocity - integer number in effort units (sum of effort units for all tasks completed during the sprint);
•	resultsDescription (optional) - string 10 - 2500 characters long, supporting Markdown syntax;
•	tasksResults - list of TaskResult for the Tasks completed in the Sprint;
•	created (generated automatically) - time stamp of the moment the entity was created;
•	modified (generated automatically) - time stamp of the moment the entity was last modified;
Each TaskResult has the following structure:
•	id (generated automatically) - long number;
•	task - the Task this result is reported for;
•	actualEffort - integer number in effort units;
•	verifiedBy - the ProductOwner" or Developer that has verified the Task successful completion;
•	resultsDescription (optional) - string 10 - 2500 characters long, supporting Markdown syntax;
•	created (generated automatically) - time stamp of the moment the entity was created;
•	modified (generated automatically) - time stamp of the moment the entity was last modified;
The Projects and Tasks inside a Project should be able to be found by browsing or by searching in title and tags fields.
In addition to the above described functionality, the web application should allow to manage (add, browse, view, edit, and delete)
different types of Users, Projects, Sprints, Tasks, ProjectResults, SprintResults, and TaskResults, according to the privileges (roles)
of the logged-in User, as described above. All input data should be validated according to the above specified requirements.
For further questions and clarification - please ask the administrator.

