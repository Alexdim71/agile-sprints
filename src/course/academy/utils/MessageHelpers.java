package course.academy.utils;

public class MessageHelpers {
    public static final String ENTITY_NOT_EXISTS = "%s with id = %d does not exist.";

    public static final String INVALID_PASSWORD = "Password must be %d to %d characters long, at least one digit, one capital letter, " +
            "and one sign different than letter or digit.";
    public static final String UNAUTHORISED_RECIPE_MODIFICATION = "Only the author or administrator may update or delete recipes.";
    public static final String UNAUTHORISED_USER_MODIFICATION = "Only the owner or administrator may update or delete users.";
    public static final String UNAUTHORISED_PROJECT_MODIFICATION = "Only an administrator may update or delete projects.";



    public static final String ABOUT = "The main goal of the project is to implement a web application allowing the Users (Developers, Product Owners, and Administrators)\n" +
            "to define and manage new Projects, Sprints and Tasks, as well as to report their progress. \n" +
            "More information about Agile Software Development can be found here: https://en.wikipedia.org/wiki/Agile_software_development.\n" +
            "All Users should have following common attributes:\n" +
            "•\tid (generated automatically) - long number;\n" +
            "•\tfirstName - string 2 to 15 characters long;\n" +
            "•\tlastName - string 2 to 15 characters long;\n" +
            "•\temail - should be valid email address, unique within the system, cannot be changed;\n" +
            "•\tusername - string 2 to 15 characters long - word characters only, unique within the system, cannot be changed;\n" +
            "•\tpassword - string 8 to 15 characters long, at least one digit, one capital letter, and one sign different than letter or digit, NOT sent back to the User clients for security reasons;\n" +
            "•\troles - DEVELOPER, PRODUCT_OWNER or ADMIN Enumerated Set (more than one role can be assigned to the User), created and editable only by Administrators;\n" +
            "•\tcontacts - string 10 - 250 characters long;\n" +
            "•\tstatus - validity status of the user account (ACTIVE, CHANGE_PASSWORD - default, users should change password on first login, SUSPENDED, or DEACTIVATED), visible and editable only by Administrators;\n" +
            "•\tcreated (generated automatically) - time stamp of the moment the User account was created;\n" +
            "•\tmodified (generated automatically) - time stamp of the moment the User account was last modified;\n" +
            "The Administrators do not have additional fields. The Developers should have in addition to the User's attributes, the following fields::\n" +
            "•\tassignedTasks - list of all tasks currently assigned;\n" +
            "•\tcompletedTaskResults - list of TaskResults for the tasks already completed by the Developer;\n" +
            "The ProductOwner should have in addition to the User's attributes, the following fields:\n" +
            "•\tprojects - list of currently assigned projects the ProductOwner is responsible for;\n" +
            "•\tcompletedProjectResults - list of ProjectResults for the projects already completed by the ProductOwner;\n" +
            "The Developers and ProductOwners should be able to manage their own personal data and change their passwords. The Administrators should be able to manage all User's data, except their passwords. The IDs of all entities should be generated automatically and should NOT be changeable.\n" +
            "The Administrators should be able to add new Projects, as well as to complete them after the ProjectResult has been reported by ProductOwner. The ProductOwners and Developers should be able to add new Sprints and Tasks to existing Projects, as well as to verify/report completion of TaskResults and SprintResults for different Tasks and Sprints completed by Developers.\n" +
            "Each Project has the following structure:\n" +
            "•\tid (generated automatically) - long number;\n" +
            "•\ttitle - string 2 to 120 characters long;\n" +
            "•\tstartDate - the start date of the Project;\n" +
            "•\tdescription (optional) - string 10 - 2500 characters long, supporting Markdown syntax;\n" +
            "•\towner - the ProductOwner responsible for the project;\n" +
            "•\tdevelopers - developers implementing the Project;\n" +
            "•\tcurrentSprint - the Sprint entity representing the currently active Sprint;\n" +
            "•\tpreviousSprintResults - the completion results for previous Sprints in the Project, including TaskResults;\n" +
            "•\ttasksBacklog - list of Tasks planned for the project, new Tasks can be added/removed dynamically during the project;\n" +
            "•\ttags - string including comma separated tags, allowing to find the Publication by quick search;\n" +
            "•\tcreated (generated automatically) - time stamp of the moment the entity was created;\n" +
            "•\tmodified (generated automatically) - time stamp of the moment the entity was last modified;\n" +
            "Each Sprint has the following structure:\n" +
            "•\tid (generated automatically) - long number;\n" +
            "•\tstartDate - start date for the sprint;\n" +
            "•\tendDate - end date for the sprint;\n" +
            "•\tduration - integer, number of working days;\n" +
            "•\towner - the ProductOwner responsible for the Sprint;\n" +
            "•\tdevelopers - developers participating in the Sprint;\n" +
            "•\ttasks - list of Tasks assigned to the current Sprint;\n" +
            "•\tcompletedTaskResults - list of TaskResults for the Tasks already completed in the Sprint;\n" +
            "•\tcreated (generated automatically) - time stamp of the moment the entity was created;\n" +
            "•\tmodified (generated automatically) - time stamp of the moment the entity was last modified;\n" +
            "Each Task has the following structure:\n" +
            "•\tid (generated automatically) - long number;\n" +
            "•\tkind - enumeration: RESEARCH, DESIGN, PROTOTYPING, IMPLEMENTATION, QA, OPERATIONS, BUG_FIXING, DOCUMENTATION, OTHER;\n" +
            "•\ttitle - string 2 to 120 characters long;\n" +
            "•\taddedBy - the User that has added the Task;\n" +
            "•\testimatedEffort - integer number in effort units \n" +
            "(the same units in which the team velocity is estimated - http://wiki.c2.com/?IdealProgrammingTime) ;\n" +
            "•\tstatus - enumeration PLANNED, ACTIVE, COMPLETED;\n" +
            "•\tsprint (optional) - the Sprint the Task belongs, if already assigned (status ACTIVE or COMPLETED);\n" +
            "•\tdevelopersAssigned - list of Developers assigned to the Task;\n" +
            "•\tdescription (optional) - string 150 - 2500 characters long, supporting Markdown syntax;\n" +
            "•\ttags - string including comma separated tags, allowing to find the Task by quick search;\n" +
            "•\tcreated (generated automatically) - time stamp of the moment the entity was created;\n" +
            "•\tmodified (generated automatically) - time stamp of the moment the entity was last modified;\n" +
            "Each ProjectResult has the following structure:\n" +
            "•\tid (generated automatically) - long number;\n" +
            "•\tproject - the Project this result is reported for;\n" +
            "•\tendDate - end date for the project;\n" +
            "•\tduration - integer, number of working days for the project;\n" +
            "•\tresultsDescription (optional) - string 10 - 2500 characters long, supporting Markdown syntax;\n" +
            "•\tsprintResults - list of SprintResult for the Sprints completed;\n" +
            "•\tcreated (generated automatically) - time stamp of the moment the entity was created;\n" +
            "•\tmodified (generated automatically) - time stamp of the moment the entity was last modified;\n" +
            "Each SprintResult has the following structure:\n" +
            "•\tid (generated automatically) - long number;\n" +
            "•\tsprint - the Sprint this result is reported for;\n" +
            "•\tteamVelocity - integer number in effort units (sum of effort units for all tasks completed during the sprint);\n" +
            "•\tresultsDescription (optional) - string 10 - 2500 characters long, supporting Markdown syntax;\n" +
            "•\ttasksResults - list of TaskResult for the Tasks completed in the Sprint;\n" +
            "•\tcreated (generated automatically) - time stamp of the moment the entity was created;\n" +
            "•\tmodified (generated automatically) - time stamp of the moment the entity was last modified;\n" +
            "Each TaskResult has the following structure:\n" +
            "•\tid (generated automatically) - long number;\n" +
            "•\ttask - the Task this result is reported for;\n" +
            "•\tactualEffort - integer number in effort units;\n" +
            "•\tverifiedBy - the ProductOwner\" or Developer that has verified the Task successful completion;\n" +
            "•\tresultsDescription (optional) - string 10 - 2500 characters long, supporting Markdown syntax;\n" +
            "•\tcreated (generated automatically) - time stamp of the moment the entity was created;\n" +
            "•\tmodified (generated automatically) - time stamp of the moment the entity was last modified;\n" +
            "The Projects and Tasks inside a Project should be able to be found by browsing or by searching in title and tags fields.\n" +
            "In addition to the above described functionality, the web application should allow to manage (add, browse, view, edit, and delete) \n" +
            "different types of Users, Projects, Sprints, Tasks, ProjectResults, SprintResults, and TaskResults, according to the privileges (roles)\n" +
            "of the logged-in User, as described above. All input data should be validated according to the above specified requirements.\n" +
            "For further questions and clarification - please ask the administrator.\n" +
            "\n";

}
