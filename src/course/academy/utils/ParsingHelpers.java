package course.academy.utils;

import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class ParsingHelpers {

    public static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    public static DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");


    public static long userInputNumber(String message) {
        Scanner sc = new Scanner(System.in);
        int check = 0;
        long number = 0;
        while (check == 0) {
            System.out.println(message);
            int ans = 0;
            try {
                ans = Integer.parseInt(sc.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Invalid format - positive numbers only.");
                continue;
            }
            if (ans <= 0) {
                System.out.println("Invalid format - positive numbers only.");
            } else {
                number = ans;
                check = 1;
            }
        }
        return number;
    }

}
