package course.academy;


import course.academy.controller.LoginController;
import course.academy.controller.MainController;
import course.academy.dao.*;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.dao.impl.DaoFactoryMemoryImpl;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.model.mapper.*;
import course.academy.service.*;
import course.academy.service.impl.ServiceFactoryImpl;

import static course.academy.model.MockEntities.*;

public class Main {
    public static final String TASKS_DB_FILENAME = "tasks.db";
    public static void main(String[] args) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException {

        //create factories, repositories and services
        DaoFactory daoFactory = new DaoFactoryMemoryImpl();
        ServiceFactory serviceFactory = new ServiceFactoryImpl();
        UserRepository userRepository = daoFactory.createUserRepository();
        UserService userService = serviceFactory.createUserService(userRepository);
        TaskRepository taskRepository = daoFactory.createTaskRepository();
        ProjectRepository projectRepository = daoFactory.createProjectRepository();
        ProjectService projectService = serviceFactory.createProjectService(projectRepository, userRepository);
//        TaskRepository taskRepository = daoFactory.createTaskFileRepository(TASKS_DB_FILENAME);
        TaskService taskService = serviceFactory.createTaskService(taskRepository, projectRepository);
        TaskResultRepository taskResultRepository = daoFactory.createTaskResultRepository();
        TaskResultService taskResultService = serviceFactory.createTaskResultService(taskResultRepository);
        SprintRepository sprintRepository = daoFactory.createSprintRepository();
        SprintService sprintService = serviceFactory.createSprintService(sprintRepository, taskRepository, userRepository);
        SprintResultRepository sprintResultRepository = daoFactory.createSprintResultRepository();
        SprintResultService sprintResultService = serviceFactory.createSprintResultService(sprintResultRepository);
        ProjectResultRepository projectResultRepository = daoFactory.createProjectResultRepository();
        ProjectResultService projectResultService = serviceFactory.createProjectResultService(projectResultRepository);


        //adding users to repo
        try {
            for (User user : mockUsers) {
                userService.add(user);
            }
        } catch (InvalidEntityDataException e) {
            e.printStackTrace();
        }
        //setting user roles by admin1;
        userService.addUserRole(admin1, admin1, Role.ADMIN);
        //for easy demonstration, Role.Developer refactored to be added by default, other roles could be added/removed,
        //user2 is set with roles developer and owner, user7 only owner.
        userService.addUserRole(admin1,user2Owner, Role.PRODUCT_OWNER);;
        userService.addUserRole(admin1, user7Owner, Role.PRODUCT_OWNER);
        userService.removeUserRole(admin1, user7Owner,Role.DEVELOPER);

        //adding project1 to repo by admin
        projectService.add(admin1, project1);

        //adding developers to project by the owner or admin;
        projectService.addDeveloperToProject(user2Owner, project1, user3Developer);
        projectService.addDeveloperToProject(user2Owner, project1, user4Developer);
        projectService.addDeveloperToProject(user2Owner, project1, user5Developer);
        projectService.addDeveloperToProject(admin1, project1, user6Developer);

        //testing exception for adding non-developer
        try {
            projectService.addDeveloperToProject(admin1, project1, user7Owner);
        } catch (UnauthorisedOperationException | InvalidEntityDataException e) {
            e.printStackTrace();
        }
        //adding Role.DEVELOPER to User7
        userService.addUserRole(admin1, user7Owner, Role.DEVELOPER);
        projectService.addDeveloperToProject(admin1, project1, user7Owner);

        //testing deleting user without authorisation
        try {
            projectService.removeDeveloperFromProject(user3Developer, project1, user7Owner);
        } catch (UnauthorisedOperationException | InvalidEntityDataException e) {
            e.printStackTrace();
        }

        //deleting developer with authorisation
        projectService.removeDeveloperFromProject(user2Owner, project1, user7Owner);

        //testing exception when deleting a developer not assigned to project
        try {
            projectService.removeDeveloperFromProject(user2Owner, project1, user7Owner);
        } catch (UnauthorisedOperationException | InvalidEntityDataException e) {
            e.printStackTrace();
        }

        //adding tasks to repo
        try {
            for (Task task : mockTasks) {
                taskService.add(user2Owner, task);
            }
        } catch (InvalidEntityDataException e) {
            e.printStackTrace();
        }

        //adding sprint1 to repo
        sprintService.add(user2Owner, sprint1);

        //adding Tasks to Sprint1 by admin, owner and developer from team, exception if not
        sprintService.addTaskToSprint(admin1, sprint1, task1);
        sprintService.addTaskToSprint(user2Owner, sprint1, task2);
        sprintService.addTaskToSprint(user2Owner, sprint1, task3);
        sprintService.addTaskToSprint(user3Developer, sprint1, task2Addition);
        try {
            sprintService.addTaskToSprint(user7Owner, sprint1, task4);
        } catch (UnauthorisedOperationException | InvalidEntityDataException e) {
            e.printStackTrace();
        }

        //Printing all users
//       userService.getAll().forEach(System.out::println);

        //Printing user by id and testing exception.
        System.out.println(userService.getById(1));
        try {
            System.out.println(userService.getById(8));
        } catch (NonexistingEntityException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(userService.getById(4));

        //Testing user contacts and exceptions
        user4Developer.setContacts("Test");
        try {
            userService.update(admin1, user4Developer);
        } catch (NonexistingEntityException | InvalidEntityDataException | UnauthorisedOperationException e) {
            System.out.println(e.getMessage());
        }
        user4Developer.setContacts("Test contacts of user4 for validation purposes only.");
        userService.update(admin1, user4Developer);
        System.out.println(user4Developer.getContacts());


        System.out.println();
        System.out.println("Printing all tasks");
        taskService.getAll().forEach(System.out::println);

        System.out.println();
        projectService.setCurrentSprint(user2Owner, project1, sprint1);
        System.out.println(project1);

        System.out.println("Sprint 1 print: ");
        System.out.println(sprint1);

        System.out.println();
        user3Developer.getAssignedTask().forEach(System.out::println); //testing all assigned tasks, status changes not tested


        //testing taskResult and correct completion of tasks
        taskResultService.add(user2Owner, task1Result);
        System.out.println();
        System.out.println(task1Result); //testing print
        System.out.println(task1); //expecting status changed to COMPLETE after adding the task result
        System.out.println();
        System.out.println("User3 tasks after finishing task1:"); //expecting task1 to be no more in the list
        user3Developer.getAssignedTask().forEach(System.out::println);
        System.out.println();
        System.out.println("User4 (another user from the team) tasksResult after finishing task1:");
        System.out.println(user4Developer.getCompletedTaskResults()); //expecting task1result to appear

        //completing 2 more tasks with results, one remaining

        taskResultService.add(user3Developer, task2Result);
        taskResultService.add(user4Developer, task2AdditionResult);

        //exception expected as no all tasks are completed.
        try {
            sprintResultService.add(user2Owner, sprint1Result);
        } catch (InvalidEntityDataException | UnauthorisedOperationException e) {
            System.out.println(e.getMessage());
        }
        //admin includes to fix the above
        taskResultService.add(admin1, task3Result);

        sprintResultService.add(user2Owner, sprint1Result); //successfully adding, testing below the print
        System.out.println();
        System.out.println(sprint1Result);
        System.out.println();
        System.out.println(sprint1); //testing print with tasks results


        //adding Tasks to Sprint2 and setting the current sprint
        sprintService.add(user2Owner, sprint2);
        sprintService.addTaskToSprint(admin1, sprint2, task4);
        sprintService.addTaskToSprint(user2Owner, sprint2, task5);
        sprintService.addTaskToSprint(user2Owner, sprint2, task5Addition);
        sprintService.addTaskToSprint(user3Developer, sprint2, task6);

        projectService.setCurrentSprint(user2Owner, project1, sprint2);
        //trying to add completed task, exception is expected
        try {
            sprintService.addTaskToSprint(admin1, sprint2, task3);
        } catch (UnauthorisedOperationException | InvalidEntityDataException e) {
            System.out.println(e.getMessage());
        }

        sprintService.removeDeveloperFromSprint(user2Owner, sprint2, user5Developer);
        sprintService.addDeveloperToSprint(user2Owner, sprint2, user6Developer);

        System.out.println(); //checking user5 tasks
        System.out.println("Testing tasks of users after removing user5 and adding user6 to Sprint2 - user5 is expected to have no currently assigned tasks.");
        System.out.println(user5Developer);
        System.out.println(user6Developer);

        //adding taskResults to Sprint2
        taskResultService.add(user2Owner, task4Result);
        taskResultService.add(user2Owner, task5Result);
        taskResultService.add(user6Developer, task5AdditionResult);
        taskResultService.add(user6Developer, task6Result);

        //adding Sprint result
        sprintResultService.add(user2Owner, sprint2Result);

        System.out.println();
        System.out.println(sprint2Result);
        System.out.println(user6Developer); //expecting no current tasks

        System.out.println();
        System.out.println(project1); //print of project after all sprints and results

        projectResultService.add(user2Owner, projectResult1);

        System.out.println();
        System.out.println(projectResult1); //testing project result print

        System.out.println();
        System.out.println("Testing exception if trying duplicate username or email:");
        try {
            userService.add(user8Test);
        } catch (InvalidEntityDataException e) {
            System.out.println(e.getMessage());
        }

        var userMapper = new UserMapper(userService);
        var projectMapper = new ProjectMapper(userService, projectService, taskService);
        var taskMapper = new TaskMapper(taskService, projectService);
        var sprintMapper = new SprintMapper(userService, projectService, sprintService);
        var projectResultMapper = new ProjectResultMapper(projectResultService, projectService);
        var taskResultMapper = new TaskResultMapper(taskService, taskResultService);
        var sprintResultMapper = new SprintResultMapper(sprintResultService, sprintService);
        var loginController = new LoginController(userService, userMapper, projectService, projectMapper,
                taskService, taskMapper, sprintService, sprintMapper, projectResultService, projectResultMapper,
                taskResultService, taskResultMapper, sprintResultService, sprintResultMapper);
        loginController.init();

    }

}
