package course.academy.exception;

public class UnauthorisedOperationException extends Exception{

    public UnauthorisedOperationException() {
    }

    public UnauthorisedOperationException(String message) {
        super(message);
    }

    public UnauthorisedOperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthorisedOperationException(Throwable cause) {
        super(cause);
    }
}
