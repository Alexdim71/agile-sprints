package course.academy.dao;


import course.academy.model.User;

import java.util.Optional;

/**
 * Public interface for managing lifecycle of User Objects
 */
public interface UserRepository extends Repository<Long, User> {



}
