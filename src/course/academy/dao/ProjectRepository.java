package course.academy.dao;

import course.academy.dao.Repository;
import course.academy.model.Project;

public interface ProjectRepository extends Repository<Long, Project> {
}
