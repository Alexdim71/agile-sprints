package course.academy.dao;

import course.academy.model.Sprint;

public interface SprintRepository extends Repository<Long, Sprint> {
}
