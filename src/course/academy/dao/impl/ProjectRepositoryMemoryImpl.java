package course.academy.dao.impl;


import course.academy.dao.ProjectRepository;
import course.academy.model.Project;

class ProjectRepositoryMemoryImpl extends AbstractMemoryLongRepository<Project> implements ProjectRepository {
    public ProjectRepositoryMemoryImpl() {
    }
}
