package course.academy.dao.impl;


import course.academy.dao.SprintResultRepository;
import course.academy.model.SprintResult;

class SprintResultRepositoryMemoryImpl extends AbstractMemoryLongRepository<SprintResult> implements SprintResultRepository {
    public SprintResultRepositoryMemoryImpl() {
    }
}
