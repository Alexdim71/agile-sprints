package course.academy.dao.impl;


import course.academy.dao.TaskResultRepository;
import course.academy.model.TaskResult;

class TaskResultRepositoryMemoryImpl extends AbstractMemoryLongRepository<TaskResult> implements TaskResultRepository {
    public TaskResultRepositoryMemoryImpl() {
    }
}
