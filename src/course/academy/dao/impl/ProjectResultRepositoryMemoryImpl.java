package course.academy.dao.impl;


import course.academy.dao.ProjectResultRepository;
import course.academy.model.ProjectResult;

class ProjectResultRepositoryMemoryImpl extends AbstractMemoryLongRepository<ProjectResult> implements ProjectResultRepository {
    public ProjectResultRepositoryMemoryImpl() {
    }
}
