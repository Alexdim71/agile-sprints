package course.academy.dao.impl;

import course.academy.dao.UserRepository;
import course.academy.model.User;

import java.util.Optional;

class UserRepositoryMemoryImpl extends AbstractMemoryLongRepository<User> implements UserRepository {
    public UserRepositoryMemoryImpl() {
    }

}
