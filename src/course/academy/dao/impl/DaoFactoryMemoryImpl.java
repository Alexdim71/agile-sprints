package course.academy.dao.impl;


import course.academy.dao.*;

public class DaoFactoryMemoryImpl implements DaoFactory {

    @Override
    public UserRepository createUserRepository() {
        return new UserRepositoryMemoryImpl();
    }

    @Override
    public TaskRepository createTaskRepository() {
        return new TaskRepositoryMemoryImpl();
    }

    @Override
    public TaskResultRepository createTaskResultRepository() {
        return new TaskResultRepositoryMemoryImpl();
    }

    @Override
    public SprintRepository createSprintRepository() {
        return new SprintRepositoryMemoryImpl();
    }

    @Override
    public SprintResultRepository createSprintResultRepository() {
        return new SprintResultRepositoryMemoryImpl();
    }

    @Override
    public ProjectRepository createProjectRepository() {
        return new ProjectRepositoryMemoryImpl();
    }

    @Override
    public ProjectResultRepository createProjectResultRepository() {
        return new ProjectResultRepositoryMemoryImpl();
    }

    @Override
    public TaskRepository createTaskFileRepository(String dbFileName) {
        return new TaskRepositoryFileImpl(new LongIdGenerator(), dbFileName);
    }


}
