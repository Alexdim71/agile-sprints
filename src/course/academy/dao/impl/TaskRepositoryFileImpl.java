package course.academy.dao.impl;

import course.academy.dao.TaskRepository;
import course.academy.dao.IdGenerator;
import course.academy.model.Task;


public class TaskRepositoryFileImpl extends PersistableRepositoryFileImpl<Long, Task>
implements TaskRepository{

    public TaskRepositoryFileImpl(IdGenerator<Long> idGenerator, String dbFileName) {
        super(idGenerator, dbFileName);
    }
}

