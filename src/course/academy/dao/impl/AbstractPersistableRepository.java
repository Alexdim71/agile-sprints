package course.academy.dao.impl;

import course.academy.dao.IdGenerator;
import course.academy.dao.PersistableRepository;
import course.academy.exception.NonexistingEntityException;
import course.academy.model.Identifiable;

import java.util.*;

public abstract class AbstractPersistableRepository<K,V extends Identifiable<K>>
        implements PersistableRepository<K,V> {
    private Map<K, V> entities = new HashMap<>();
    private IdGenerator<K> idGenerator;

    public AbstractPersistableRepository(IdGenerator<K> idGenerator) {
        this.idGenerator = idGenerator;
    }

    @Override
    public Collection<V> findAll() {
        return entities.values();
    }

    public List<V> findAllSorted(Comparator<V> comparator) {
        var sorted = new ArrayList<>(entities.values());
        sorted.sort(comparator);
        return sorted;
    }

    @Override
    public V findById(K id) {
        return entities.get(id);
    }


    @Override
    public V create(V entity) {
        entity.setId(idGenerator.getNextId());
        entities.put(entity.getId(), entity);
        return entity;
    }


    public void addAll(Collection<V> entities) {
        for(var entity: entities) {
            this.entities.put(entity.getId(), entity);
        }
    }


    public void clear() {
        entities.clear();
    }

    @Override
    public V update(V entity) throws NonexistingEntityException {
        V old = findById(entity.getId());
        if(old == null) {
            throw new NonexistingEntityException("Book with ID='" + entity.getId() + "' does not exist.");
        }
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public V deleteById(K id) throws NonexistingEntityException {
        V old = entities.remove(id);
        if(old == null) {
            throw new NonexistingEntityException("Book with ID='" + id + "' does not exist.");
        }
        return old;
    }

    @Override
    public long count() {
        return entities.size();
    }

    public IdGenerator<K> getIdGenerator() {
        return idGenerator;
    }

    public void setIdGenerator(IdGenerator<K> idGenerator) {
        this.idGenerator = idGenerator;
    }
}










