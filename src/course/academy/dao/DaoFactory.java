package course.academy.dao;

public interface DaoFactory {

    UserRepository createUserRepository();
    TaskRepository createTaskRepository();
    TaskResultRepository createTaskResultRepository();
    SprintRepository createSprintRepository();
    SprintResultRepository createSprintResultRepository();
    ProjectRepository createProjectRepository();
    ProjectResultRepository createProjectResultRepository();

    TaskRepository createTaskFileRepository(String dbFileName);

}
