package course.academy.dao;

public interface Persistable {
    void load();

    void save();

}
