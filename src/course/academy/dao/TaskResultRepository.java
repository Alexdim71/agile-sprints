package course.academy.dao;

import course.academy.dao.Repository;
import course.academy.model.TaskResult;

public interface TaskResultRepository extends Repository<Long, TaskResult> {
}
