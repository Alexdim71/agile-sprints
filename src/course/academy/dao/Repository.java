package course.academy.dao;

import course.academy.exception.NonexistingEntityException;
import course.academy.model.Identifiable;

import java.util.Collection;


/**
 * Public interface for managing lifecycle of User Objects
 */
public interface Repository<K, V extends Identifiable<K>> {

    Collection<V> findAll();
    V findById(K id);
    V create(V entity);
    V update(V entity) throws NonexistingEntityException;
    V deleteById(K id) throws NonexistingEntityException;
    long count();
}
