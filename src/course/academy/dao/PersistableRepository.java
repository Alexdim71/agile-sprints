package course.academy.dao;

import course.academy.model.Identifiable;

public interface PersistableRepository<K, V extends Identifiable<K>> extends Repository<K,V>, Persistable {
}
