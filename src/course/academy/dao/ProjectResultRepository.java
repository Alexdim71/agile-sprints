package course.academy.dao;

import course.academy.model.ProjectResult;

public interface ProjectResultRepository extends Repository <Long, ProjectResult> {
}
