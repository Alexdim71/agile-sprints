package course.academy.dao;

import course.academy.model.Task;

public interface TaskRepository extends PersistableRepository<Long, Task> {
}
