package course.academy.dao;

import course.academy.model.SprintResult;

public interface SprintResultRepository extends Repository<Long, SprintResult> {
}
