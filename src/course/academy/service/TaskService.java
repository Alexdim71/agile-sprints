package course.academy.service;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Project;
import course.academy.model.StatusTask;
import course.academy.model.Task;
import course.academy.model.User;

import java.util.Collection;

public interface TaskService {
//    void loadData();
    Collection<Task> getAll();
    Task getById(long id) throws NonexistingEntityException;
    Task add(User loggedUser, Task task) throws InvalidEntityDataException, UnauthorisedOperationException, NonexistingEntityException;
    Task update(User loggedUser, Task task) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException;
    Task deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException;
    long count();
    StatusTask updateStatusTask(User loggedUser, Task task, StatusTask status) throws UnauthorisedOperationException, NonexistingEntityException;
    Collection<Task> searchTask(String searchString);

}
