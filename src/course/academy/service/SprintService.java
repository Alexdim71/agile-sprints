package course.academy.service;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Sprint;
import course.academy.model.Task;
import course.academy.model.User;

import java.util.Collection;

public interface SprintService {
    Collection<Sprint> getAll();
    Sprint getById(long id) throws NonexistingEntityException;
    Sprint add(User loggedUser, Sprint sprint) throws InvalidEntityDataException, UnauthorisedOperationException;
    Sprint update(User loggedUser, Sprint sprint) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException;
    Sprint deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException;
    Task addTaskToSprint(User loggedUser, Sprint sprint, Task task) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException;
    Task removeTaskFromSprint(User loggedUser, Sprint sprint, Task task) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException;
    User addDeveloperToSprint(User loggedUser, Sprint sprint, User user) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException;
    User removeDeveloperFromSprint(User loggedUser, Sprint sprint, User user) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException;

    long count();
}
