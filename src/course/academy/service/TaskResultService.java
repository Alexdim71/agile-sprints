package course.academy.service;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.TaskResult;
import course.academy.model.User;

import java.util.Collection;

public interface TaskResultService {
    Collection<TaskResult> getAll();
    TaskResult getById(long id) throws NonexistingEntityException;
    TaskResult add(User loggedUser, TaskResult taskResult) throws InvalidEntityDataException, UnauthorisedOperationException;
    TaskResult update(User loggedUser, TaskResult taskResult) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException;
    TaskResult deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException;
    long count();
}
