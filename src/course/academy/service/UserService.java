package course.academy.service;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Role;
import course.academy.model.StatusUser;
import course.academy.model.User;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface UserService {
    Collection<User> getAll();
    Optional<User> findByUsername (String username);
    Optional<User> findByEmail (String email);
    User getById(long id) throws NonexistingEntityException;
    User add(User user) throws InvalidEntityDataException;
    User update(User loggedUser, User user) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException;
    User deleteById(User user, long id) throws NonexistingEntityException, UnauthorisedOperationException;
    long count();
    void addUserRole(User loggedUser, User user, Role role) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException;
    void removeUserRole(User loggedUser, User user, Role role) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException;
    void setUserStatus(User loggedUser, User user, StatusUser status) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException;
    StatusUser getUserStatus (User loggedUser, User user) throws UnauthorisedOperationException;
    void changePassword(User loggedUser, User user, String password) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException;
}
