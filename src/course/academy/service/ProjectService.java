package course.academy.service;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Project;
import course.academy.model.Sprint;
import course.academy.model.Task;
import course.academy.model.User;

import java.util.Collection;

public interface ProjectService {
    Collection<Project> getAll();
    Project getById(long id) throws NonexistingEntityException;
    Project add(User loggedUser, Project project) throws InvalidEntityDataException, UnauthorisedOperationException, NonexistingEntityException;
    Project update(User loggedUser, Project project) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException;
    Project deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException;
    long count();
    User addDeveloperToProject(User loggedUser, Project project, User user) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException;
    User removeDeveloperFromProject(User loggedUser, Project project, User user) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException;
    Sprint setCurrentSprint(User loggedUser, Project project, Sprint sprint) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException;
    Collection<Project> searchProject (String searchString);
}
