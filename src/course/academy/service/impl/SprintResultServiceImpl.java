package course.academy.service.impl;

import course.academy.dao.SprintResultRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.service.SprintResultService;

import java.time.LocalDateTime;
import java.util.Collection;

import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.SprintResult.RESULTS_DESCRIPTION_MIN_LENGTH;
import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;

class SprintResultServiceImpl implements SprintResultService {
    private final SprintResultRepository sprintResultRepository;

    SprintResultServiceImpl(SprintResultRepository sprintResultRepository) {
        this.sprintResultRepository = sprintResultRepository;
    }


    @Override
    public Collection<SprintResult> getAll() {
        return sprintResultRepository.findAll();
    }

    @Override
    public SprintResult getById(long id) throws NonexistingEntityException {
        var sprintResult = sprintResultRepository.findById(id);
        if (sprintResult == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "Sprint", id));
        }
        return sprintResult;
    }

    @Override
    public SprintResult add(User loggedUser, SprintResult sprintResult) throws InvalidEntityDataException, UnauthorisedOperationException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(sprintResult.getSprint().getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may add sprint results to projects.");
        }
        validateSprintResult(sprintResult);
        Project project = sprintResult.getSprint().getProject();
        for (Task task: sprintResult.getSprint().getTasks()){
            if (!task.getStatusTask().equals(StatusTask.COMPLETED)) {
                throw new InvalidEntityDataException("All tasks in the sprints must be with Status complete before drafting a sprint result");
            }
        }
        int teamVelocity = calculateTeamVelocity(sprintResult);
        sprintResult.setTeamVelocity(teamVelocity);
        sprintResultRepository.create(sprintResult);
        project.getPreviousSprintResults().add(sprintResult);
        return sprintResult;
    }

    @Override
    public SprintResult update(User loggedUser, SprintResult sprintResult) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException {
        validateSprintResult(sprintResult);
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(sprintResult.getSprint().getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may update sprint results.");
        }
        int teamVelocity = calculateTeamVelocity(sprintResult);
        sprintResult.setTeamVelocity(teamVelocity);
        sprintResult.setModified(LocalDateTime.now());
        return sprintResultRepository.update(sprintResult);
    }

    @Override
    public SprintResult deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException {
        SprintResult sprintResult = getById(id);
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(sprintResult.getSprint().getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may delete sprint results.");
        }
        Project project = sprintResult.getSprint().getProject();
        sprintResultRepository.deleteById(id);
        project.getPreviousSprintResults().remove(sprintResult);
        return sprintResult;
    }

    @Override
    public long count() {
        return sprintResultRepository.count();
    }

    private void validateSprintResult(SprintResult sprintResult) throws InvalidEntityDataException {
        if (sprintResult.getResultsDescription() != null) {
            int descriptionLength = sprintResult.getResultsDescription().length();
            if (descriptionLength < RESULTS_DESCRIPTION_MIN_LENGTH || descriptionLength > RESULTS_DESCRIPTION_MAX_LENGTH) {
                throw new InvalidEntityDataException(String.format("Project results description must be between %d and %d characters.",
                        RESULTS_DESCRIPTION_MIN_LENGTH, RESULTS_DESCRIPTION_MAX_LENGTH));
            }
        }
    }

    private int calculateTeamVelocity(SprintResult sprintResult) {
        var totalTeamEffortUnits = sprintResult.getSprint().getCompletedTaskResults()
                .stream()
                .mapToInt(TaskResult::getActualEffort)
                .sum();

        return totalTeamEffortUnits / sprintResult.getSprint().getDuration();
    }
}
