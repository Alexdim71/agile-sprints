package course.academy.service.impl;

import course.academy.dao.TaskResultRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.service.TaskResultService;

import java.time.LocalDateTime;
import java.util.Collection;

import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.TaskResult.RESULT_DESCRIPTION_MIN_LENGTH;
import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;

class TaskResultServiceImpl implements TaskResultService {
    private final TaskResultRepository taskResultRepository;

    public TaskResultServiceImpl(TaskResultRepository taskResultRepository) {
        this.taskResultRepository = taskResultRepository;
    }

    @Override
    public Collection<TaskResult> getAll() {
        return taskResultRepository.findAll();
    }

    @Override
    public TaskResult getById(long id) throws NonexistingEntityException {
        var taskResult = taskResultRepository.findById(id);
        if (taskResult == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "TaskResult", id));
        }
        return taskResult;
    }

    @Override
    public TaskResult add(User loggedUser, TaskResult taskResult) throws InvalidEntityDataException, UnauthorisedOperationException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(taskResult.getTask().getProject().getOwner()) &&
        !taskResult.getTask().getDevelopersAssigned().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or team developer may add tasks results to projects.");
        }
        validateTaskResult(taskResult);
        Task task = taskResult.getTask();
        if (task.getSprint() == null) {
            throw new InvalidEntityDataException("Task should be assigned to sprint and completed in order to write task result.");
        }
        taskResult.setVerifiedBy(loggedUser);
        taskResultRepository.create(taskResult);
        task.setStatusTask(StatusTask.COMPLETED);
        Sprint sprint = taskResult.getTask().getSprint();
        sprint.getCompletedTaskResults().add(taskResult);
        for (User developer: sprint.getDevelopers()) {
            developer.getCompletedTaskResults().add(taskResult);
            developer.getAssignedTask().remove(task);
        }
        return taskResult;
    }

    @Override
    public TaskResult update(User loggedUser, TaskResult taskResult) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(taskResult.getTask().getProject().getOwner()) &&
                !taskResult.getTask().getDevelopersAssigned().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or team developer may add tasks results to projects.");
        }
        validateTaskResult(taskResult);
        taskResult.setModified(LocalDateTime.now());
        taskResultRepository.update(taskResult);
        return taskResult;
    }

    @Override
    public TaskResult deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException {
        TaskResult taskResult = getById(id);
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(taskResult.getTask().getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may update tasks results.");
        }
        taskResultRepository.deleteById(id);
        Sprint sprint = taskResult.getTask().getSprint();
        sprint.getCompletedTaskResults().remove(taskResult);
        for (User developer: sprint.getDevelopers()) {
            developer.getCompletedTaskResults().remove(taskResult);
        }

        return taskResult;
    }

    @Override
    public long count() {
        return taskResultRepository.count();
    }

    private void validateTaskResult(TaskResult taskResult) throws InvalidEntityDataException {
        if (taskResult.getActualEffort() < 0) {
            throw new InvalidEntityDataException("Actual effort must be positive integer value.");
        }
        if (taskResult.getResultDescription() != null) {
            int descriptionLength = taskResult.getResultDescription().length();
            if (descriptionLength < RESULT_DESCRIPTION_MIN_LENGTH || descriptionLength > RESULT_DESCRIPTION_MAX_LENGTH) {
                throw new InvalidEntityDataException(String.format("Task result description must be between %d and %d characters.",
                        RESULT_DESCRIPTION_MIN_LENGTH, RESULT_DESCRIPTION_MAX_LENGTH));
            }
        }

    }
}
