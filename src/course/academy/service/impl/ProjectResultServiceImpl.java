package course.academy.service.impl;

import course.academy.dao.ProjectResultRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Project;
import course.academy.model.ProjectResult;
import course.academy.model.Role;
import course.academy.model.User;
import course.academy.service.ProjectResultService;

import java.time.LocalDateTime;
import java.util.Collection;

import static course.academy.model.ProjectResult.RESULTS_DESCRIPTION_MAX_LENGTH;
import static course.academy.model.ProjectResult.RESULTS_DESCRIPTION_MIN_LENGTH;
import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;

class ProjectResultServiceImpl implements ProjectResultService {
    private final ProjectResultRepository projectResultRepository;

    ProjectResultServiceImpl(ProjectResultRepository projectResultRepository) {
        this.projectResultRepository = projectResultRepository;
    }

    @Override
    public Collection<ProjectResult> getAll() {
        return projectResultRepository.findAll();
    }

    @Override
    public ProjectResult getById(long id) throws NonexistingEntityException {
        var projectResult = projectResultRepository.findById(id);
        if (projectResult == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "ProjectResult", id));
        }
        return projectResult;
    }

    @Override
    public ProjectResult add(User loggedUser, ProjectResult projectResult) throws InvalidEntityDataException, UnauthorisedOperationException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(projectResult.getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may create the project result.");
        }
        validateProjectResult(projectResult);
        return projectResultRepository.create(projectResult);
    }

    @Override
    public ProjectResult update(User loggedUser, ProjectResult projectResult) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException {
        validateProjectResult(projectResult);
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(projectResult.getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may update the project result.");
        }
        projectResult.setModified(LocalDateTime.now());
        return projectResultRepository.update(projectResult);
    }

    @Override
    public ProjectResult deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException {
        ProjectResult projectResult = getById(id);
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(projectResult.getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may create the project result.");
        }
        return projectResultRepository.deleteById(id);
    }

    @Override
    public long count() {
        return projectResultRepository.count();
    }

    private void validateProjectResult(ProjectResult projectResult) throws InvalidEntityDataException {
        if (projectResult.getResultsDescription() != null) {
            int descriptionLength = projectResult.getResultsDescription().length();
            if (descriptionLength < RESULTS_DESCRIPTION_MIN_LENGTH || descriptionLength > RESULTS_DESCRIPTION_MAX_LENGTH) {
                throw new InvalidEntityDataException(String.format("Project results description must be between %d and %d characters.",
                        RESULTS_DESCRIPTION_MIN_LENGTH, RESULTS_DESCRIPTION_MAX_LENGTH));
            }
        }
        if (projectResult.getEndDate().isBefore(projectResult.getProject().getStartDate())) {
            throw new InvalidEntityDataException("The end date must be after the project start");
        }
    }

}



