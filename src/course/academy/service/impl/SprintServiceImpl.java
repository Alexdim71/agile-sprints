package course.academy.service.impl;

import course.academy.dao.SprintRepository;
import course.academy.dao.TaskRepository;
import course.academy.dao.UserRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.service.SprintService;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;

class SprintServiceImpl implements SprintService {
    private final SprintRepository sprintRepository;
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;

    SprintServiceImpl(SprintRepository sprintRepository, TaskRepository taskRepository, UserRepository userRepository) {
        this.sprintRepository = sprintRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Collection<Sprint> getAll() {
        return sprintRepository.findAll();
    }

    @Override
    public Sprint getById(long id) throws NonexistingEntityException {
        var sprint = sprintRepository.findById(id);
        if (sprint == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "Sprint", id));
        }
        return sprint;
    }

    @Override
    public Sprint add(User loggedUser, Sprint sprint) throws InvalidEntityDataException, UnauthorisedOperationException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(sprint.getProject().getOwner())
                && !sprint.getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner or assigned developers may add sprints.");
        }
        validateSprintUsersAndTasks(sprint);
        sprint.setOwner(loggedUser);
        return sprintRepository.create(sprint);
    }

    @Override
    public Sprint update(User loggedUser, Sprint sprint) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(sprint.getProject().getOwner())
                && !sprint.getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner or assigned developers may update sprints.");
        }
        validateSprintUsersAndTasks(sprint);
        sprint.setModified(LocalDateTime.now());
        return sprintRepository.update(sprint);
    }

    @Override
    public Sprint deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException {
        Sprint sprint = getById(id);
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(sprint.getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may delete sprints.");
        }
        return sprintRepository.deleteById(id);
    }

    @Override
    public Task addTaskToSprint(User loggedUser, Sprint sprint, Task task) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(task.getProject().getOwner()) &&
                !task.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may add tasks to sprints.");
        }

        validateSprintTask(sprint, task);
        if (sprint.getTasks().contains(task)) {
            throw new InvalidEntityDataException(
                    String.format("Task with ID %d is already included in the sprint", task.getId()));
        }

        List<Task> tasks = sprint.getTasks();
        tasks.add(task);
        sprint.setTasks(tasks);
        for (User developer : sprint.getDevelopers()) {
            List<Task> assignedTasks = developer.getAssignedTask();
            assignedTasks.add(task);
            developer.setAssignedTask(assignedTasks);
            developer.setModified(LocalDateTime.now());
            userRepository.update(developer);
        }
        task.setSprint(sprint);
        task.getDevelopersAssigned().addAll(sprint.getDevelopers());
        task.setModified(LocalDateTime.now());
        taskRepository.update(task);
        sprint.setModified(LocalDateTime.now());
        sprintRepository.update(sprint);
        return task;
    }

    @Override
    public Task removeTaskFromSprint(User loggedUser, Sprint sprint, Task task) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(task.getProject().getOwner()) &&
                !task.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may remove tasks from sprints.");
        }
        if (!sprint.getTasks().contains(task)) {
            throw new InvalidEntityDataException("The task is not included in the sprint");
        }
        if (task.getStatusTask().equals(StatusTask.COMPLETED)) {
            throw new InvalidEntityDataException("The task status is completed, please update it first.");
        }

        List<Task> tasks = sprint.getTasks();
        tasks.remove(task);
        sprint.setTasks(tasks);

        for (User developer : sprint.getDevelopers()) {
            List<Task> assignedTasks = developer.getAssignedTask();
            assignedTasks.remove(task);
            developer.setAssignedTask(assignedTasks);
            developer.setModified(LocalDateTime.now());
            userRepository.update(developer);
        }

        sprint.setModified(LocalDateTime.now());
        sprintRepository.update(sprint);
        return task;
    }

    @Override
    public User addDeveloperToSprint(User loggedUser, Sprint sprint, User user) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(sprint.getProject().getOwner()) &&
                !sprint.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may add developers to tasks.");
        }

        validateSprintUser(sprint, user);
        if (sprint.getDevelopers().contains(user)) {
            throw new InvalidEntityDataException(
                    String.format("User with ID %d is already assigned to the task", user.getId()));
        }

        List<User> devs = sprint.getDevelopers();
        devs.add(user);
        sprint.setDevelopers(devs);
        sprint.setModified(LocalDateTime.now());
        sprintRepository.update(sprint);

        for (Task task : sprint.getTasks()) {
            if (!task.getStatusTask().equals(StatusTask.COMPLETED)) {
                List<User> devsAssigned = task.getDevelopersAssigned();
                devsAssigned.add(user);
                task.setDevelopersAssigned(devsAssigned);
                task.setModified(LocalDateTime.now());
                taskRepository.update(task);
                List<Task> assignedTasks = user.getAssignedTask();
                assignedTasks.add(task);
                user.setAssignedTask(assignedTasks);
                user.setModified(LocalDateTime.now());
                userRepository.update(user);
            }
        }
        return user;
    }

    @Override
    public User removeDeveloperFromSprint(User loggedUser, Sprint sprint, User user) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException {

        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(sprint.getProject().getOwner()) &&
                !sprint.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may remove developers from tasks.");
        }
        if (!sprint.getDevelopers().contains(user)) {
            throw new InvalidEntityDataException("This user is not assigned to the sprint.");
        }
        List<User> devs = sprint.getDevelopers();
        devs.remove(user);
        sprint.setDevelopers(devs);

        sprint.setModified(LocalDateTime.now());
        sprintRepository.update(sprint);

        for (Task task : sprint.getTasks()) {
            if (!task.getStatusTask().equals(StatusTask.COMPLETED)) {
                List<User> devsAssigned = task.getDevelopersAssigned();
                devsAssigned.remove(user);
                task.setDevelopersAssigned(devsAssigned);
                task.setModified(LocalDateTime.now());
                taskRepository.update(task);
                List<Task> assignedTasks = user.getAssignedTask();
                assignedTasks.remove(task);
                user.setAssignedTask(assignedTasks);
                user.setModified(LocalDateTime.now());
                userRepository.update(user);
            }
        }
        return user;
    }

    @Override
    public long count() {
        return sprintRepository.count();
    }

    private void validateSprintUsersAndTasks(Sprint sprint) throws InvalidEntityDataException {
        for (User user: sprint.getDevelopers()) {
            validateSprintUser(sprint, user);
        }
        for (Task task: sprint.getTasks()) {
            validateSprintTask(sprint, task);
        }
    }

    private void validateSprintUser(Sprint sprint, User user) throws InvalidEntityDataException {
        if (!sprint.getProject().getDevelopers().contains(user)) {
            throw new InvalidEntityDataException(
                    String.format("User with ID %d in not from the project team.", user.getId()));
        }
    }

    private void validateSprintTask(Sprint sprint, Task task) throws InvalidEntityDataException {
        if (!task.getProject().equals(sprint.getProject())) {
            throw new InvalidEntityDataException(
                    String.format("Task ID %d and the Sprint ID %d are from different projects.",
                            task.getId(), sprint.getId()));
        }

        if (!task.getStatusTask().equals(StatusTask.PLANNED)) {
            throw new InvalidEntityDataException("The task should be with status 'Planned' before adding to a sprint");
        }
    }
}



