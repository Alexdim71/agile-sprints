package course.academy.service.impl;

import course.academy.dao.ProjectRepository;
import course.academy.dao.UserRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Project;
import course.academy.model.Role;
import course.academy.model.Sprint;
import course.academy.model.User;
import course.academy.service.ProjectService;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static course.academy.model.Project.*;
import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;

class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;
    private final UserRepository userRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository, UserRepository userRepository) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }


    @Override
    public Collection<Project> getAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project getById(long id) throws NonexistingEntityException {
            var project = projectRepository.findById(id);
            if (project == null) {
                throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "Project", id));
            }
            return project;
    }

    @Override
    public Project add(User loggedUser, Project project) throws InvalidEntityDataException, UnauthorisedOperationException, NonexistingEntityException {
        validateProject(project);
        if (!loggedUser.getRoles().contains(Role.ADMIN)) {
            throw new UnauthorisedOperationException("Only an administrator is authorised to create projects.");
        }
        projectRepository.create(project);
        User owner = project.getOwner();
        List<Project> projects = owner.getProjects();
        projects.add(project);
        owner.setProjects(projects);
        owner.setModified(LocalDateTime.now());
        userRepository.update(owner);
        return project;
    }

    @Override
    public Project update(User loggedUser, Project project) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(project.getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may update projects");
        }
        validateProject(project);
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);

        User owner = project.getOwner();
        if (!owner.getProjects().contains(project)) {
            List<Project> projects = owner.getProjects();
            projects.add(project);
            owner.setProjects(projects);
            owner.setModified(LocalDateTime.now());
            userRepository.update(owner);
        }

        return project;
    }

    @Override
    public Project deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException {
        if (!loggedUser.getRoles().contains(Role.ADMIN)) {
            throw new UnauthorisedOperationException("Only an administrator is authorised to delete projects.");
        }
        Project project = getById(id);
        projectRepository.deleteById(id);
        User owner = project.getOwner();
        List<Project> projects = owner.getProjects();
        projects.remove(project);
        owner.setProjects(projects);
        owner.setModified(LocalDateTime.now());
        userRepository.update(owner);
        return project;
    }

    @Override
    public long count() {
        return projectRepository.count();
    }

    @Override
    public User addDeveloperToProject(User loggedUser, Project project, User user) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(project.getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may add users to projects");
        }
        if (!user.getRoles().contains(Role.DEVELOPER)) {
            throw new InvalidEntityDataException("The user is not a developer. Please first assign role.");
        }
        if (project.getDevelopers().contains(user)) {
            throw new InvalidEntityDataException("The user is already assigned to this project.");
        }
        List<User> developers = project.getDevelopers();
        developers.add(user);
        project.setDevelopers(developers);
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);
        return user;
    }

    @Override
    public User removeDeveloperFromProject(User loggedUser, Project project, User user) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(project.getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may add users to projects");
        }
        if (!project.getDevelopers().contains(user)) {
            throw new InvalidEntityDataException("The user is not assigned to this project.");
        }
        List<User> developers = project.getDevelopers();
        developers.remove(user);
        project.setDevelopers(developers);
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);
        return user;
    }

    @Override
    public Sprint setCurrentSprint(User loggedUser, Project project, Sprint sprint) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(project.getOwner()) &&
                !sprint.getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the sprint team may add sprints to projects.");
        }
//        The below check is tested as operational and intentionally skipped for test purposes.
//        if (sprint.getEndDate().isBefore(LocalDate.now())) {
//            throw new InvalidEntityDataException("This sprint is finished");
//        }
        if (!sprint.getProject().equals(project)) {
            throw new InvalidEntityDataException("The sprint is not assigned to this project.");
        }
        project.setCurrentSprint(sprint);
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);
        return sprint;
    }

    @Override
    public Collection<Project> searchProject (String searchString) {
        String searchWord = searchString.toLowerCase();
        return getAll().stream().filter(project -> project.getTitle().toLowerCase().contains(searchWord)
                || Arrays.stream(project.getTags().split(",")).anyMatch(pr -> pr.equalsIgnoreCase(searchWord)))
                .collect(Collectors.toList());
    }


    private void validateProject(Project project) throws InvalidEntityDataException {
        if (!project.getOwner().getRoles().contains(Role.PRODUCT_OWNER)) {
            throw new InvalidEntityDataException("The chosen user for owner has no role 'PRODUCT_OWNER'");
        }
        int titleLength = project.getTitle().length();
        if (titleLength < TITLE_MIN_LENGTH || titleLength > TITLE_MAX_LENGTH) {
            throw new InvalidEntityDataException(String.format("Title length must be between %d and %d characters.",
                    TITLE_MIN_LENGTH, TITLE_MAX_LENGTH));
        }

        if (project.getDescription() != null) {
            int descriptionLength = project.getDescription().length();
            if (descriptionLength < DESCRIPTION_MIN_LENGTH || descriptionLength > DESCRIPTION_MAX_LENGTH) {
                throw new InvalidEntityDataException(String.format("Project description must be between %d and %d characters.",
                        DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH));
            }
        }
    }

}


