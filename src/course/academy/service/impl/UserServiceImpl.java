package course.academy.service.impl;

import course.academy.dao.UserRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.service.UserService;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import static course.academy.model.User.*;
import static course.academy.utils.MessageHelpers.*;

class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Collection<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findByUsername (String username) {
          return getAll().stream().filter(u -> u.getUsername().equals(username)).findFirst();
    }

    @Override
    public Optional<User> findByEmail (String email) {
        return getAll().stream().filter(u -> u.getEmail().equals(email)).findFirst();
    }

    @Override
    public User getById(long id) throws NonexistingEntityException {
        var user = userRepository.findById(id);
        if (user == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "User", id));
        }
        return user;
    }

    @Override
    public User add(User user) throws InvalidEntityDataException {
        validateUser(user);
        Optional<User> existingUser = findByUsername(user.getUsername());
        if (existingUser.isPresent()) {
            throw new InvalidEntityDataException(String.format("User with username %s already exists", user.getUsername()));
        }
        Optional<User> existingMail = findByEmail(user.getEmail());
        if (existingMail.isPresent()) {
            throw new InvalidEntityDataException(String.format("User with email %s already exists", user.getEmail()));
        }
        return userRepository.create(user);
    }

    @Override
    public User update(User loggedUser, User user) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException {
        if (loggedUser.getId() != user.getId() && !loggedUser.getRoles().contains(Role.ADMIN)) {
            throw new UnauthorisedOperationException(UNAUTHORISED_USER_MODIFICATION);
        }
//        User userToBeUpdated = getById(user.getId());
        validateUser(user);
        user.setModified(LocalDateTime.now());
        userRepository.update(user);
        return user;
    }

    @Override
    public User deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException {
        if (loggedUser.getId() != id && !loggedUser.getRoles().contains(Role.ADMIN)) {
            throw new UnauthorisedOperationException(UNAUTHORISED_USER_MODIFICATION);
        }
        return userRepository.deleteById(id);
    }

    @Override
    public long count() {
        return userRepository.count();
    }

    @Override
    public void addUserRole(User loggedUser, User user, Role role) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && loggedUser.getId() !=1) {
            throw new UnauthorisedOperationException("The status may be changed only by Administrator.");
        }
        Set<Role> roles = user.getRoles();
        roles.add(role);
        user.setRoles(roles);
        update(loggedUser, user);
    }

    @Override
    public void removeUserRole(User loggedUser, User user, Role role) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && loggedUser.getId() !=1) {
            throw new UnauthorisedOperationException("The status may be changed only by Administrator.");
        }
        Set<Role> roles = user.getRoles();
        roles.remove(role);
        user.setRoles(roles);
        update(loggedUser, user);
    }

    @Override
    public void setUserStatus(User loggedUser, User user, StatusUser status) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && loggedUser.getId() !=1) {
            throw new UnauthorisedOperationException("The status may be changed only by Administrator.");
        }
        user.setStatusUser(status);
        update(loggedUser, user);
    }

    @Override
    public StatusUser getUserStatus(User loggedUser, User user) throws UnauthorisedOperationException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && loggedUser.getId() !=1) {
            throw new UnauthorisedOperationException("The status may be changed only by Administrator.");
        }
        return user.getStatusUser();
    }

    @Override
    public void changePassword(User loggedUser, User user, String password) throws UnauthorisedOperationException, InvalidEntityDataException, NonexistingEntityException {
        if (!loggedUser.equals(user)) {
            throw new UnauthorisedOperationException("Only the profile owner is authorised to change password");
        }
        if (!password.matches(checkPassword)) {
            throw new InvalidEntityDataException(String.format(INVALID_PASSWORD, PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH));
        }
        user.setPassword(password);
        user.setStatusUser(StatusUser.ACTIVE);
        update(loggedUser, user);

    }

    private void validateUser(User user) throws InvalidEntityDataException {
        int firstNameLength = user.getFirstName().length();
        int lastNameLength = user.getLastName().length();
        if (firstNameLength < FIRST_NAME_MIN_LENGTH || firstNameLength > FIRST_NAME_MAX_LENGTH) {
            throw new InvalidEntityDataException(String.format("First name must be with length between %d and %d characters",
                    FIRST_NAME_MIN_LENGTH, FIRST_NAME_MAX_LENGTH));
        }
        if (lastNameLength < LAST_NAME_MIN_LENGTH || lastNameLength > LAST_NAME_MAX_LENGTH) {
            throw new InvalidEntityDataException(String.format("Last name must be with length between %d and %d characters",
                    LAST_NAME_MIN_LENGTH, LAST_NAME_MAX_LENGTH));
        }

        if (!user.getUsername().matches(checkUsername)) {
            throw new InvalidEntityDataException(String.format("Username must be between %d and %d characters and contain only word characters",
                    USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH));
        }


        if (!user.getEmail().matches(checkEmail)) {
            throw new InvalidEntityDataException("This is not a valid mail address.");
        }

        if (!user.getPassword().matches(checkPassword)) {
            throw new InvalidEntityDataException(String.format(INVALID_PASSWORD, PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH));
        }

       if (user.getContacts() != null) {
           int contactsLength = user.getContacts().length();
           if (contactsLength < CONTACTS_MIN_LENGTH || contactsLength > CONTACTS_MAX_LENGTH) {
               throw new InvalidEntityDataException(String.format("Contacts must be between %d and %d characters.",
                       CONTACTS_MIN_LENGTH, CONTACTS_MAX_LENGTH));
           }
       }

    }

}
