package course.academy.service.impl;

import course.academy.dao.ProjectRepository;
import course.academy.dao.TaskRepository;
import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.*;
import course.academy.service.TaskService;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static course.academy.utils.MessageHelpers.ENTITY_NOT_EXISTS;
import static course.academy.model.Task.*;

class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;

    TaskServiceImpl(TaskRepository taskRepository, ProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

//    public void loadData() {
//        taskRepository.load();
//    }

    @Override
    public Collection<Task> getAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task getById(long id) throws NonexistingEntityException {
        var task = taskRepository.findById(id);
        if (task == null) {
            throw new NonexistingEntityException(String.format(ENTITY_NOT_EXISTS, "Task", id));
        }
        return task;
    }

    @Override
    public Task add(User loggedUser, Task task) throws InvalidEntityDataException, UnauthorisedOperationException, NonexistingEntityException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(task.getProject().getOwner()) &&
                !task.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may add tasks to projects.");
        }
        validateTask(task);
        task.setAddedBy(loggedUser);
        taskRepository.create(task);
//        taskRepository.save(); //for file persistence purpose, doing nothing in Memory Repository
        Project project = task.getProject();
        List<Task> taskList = project.getTasksBacklog();
        taskList.add(task);
        project.setTasksBacklog(taskList);
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);
        return task;
    }

    @Override
    public Task update(User loggedUser, Task task) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(task.getProject().getOwner()) &&
                !task.getProject().getDevelopers().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the project team may update tasks.");
        }
        validateTask(task);
        task.setModified(LocalDateTime.now());
        taskRepository.update(task);

//        taskRepository.save();

//        If project is not changed, which should be avoided, below check is skipped.
//        Project project = task.getProject();
//        List<Task> taskList = project.getTasksBacklog();
//        taskList.add(task);
//        project.setTasksBacklog(taskList);
//        project.setModified(LocalDateTime.now());
//        projectRepository.update(project);

        return task;
    }

    @Override
    public Task deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException {
        Task task = getById(id);
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(task.getProject().getOwner())) {
            throw new UnauthorisedOperationException("Only an administrator or the project owner may delete tasks.");
        }
        Project project = task.getProject();
        taskRepository.deleteById(id);
//        taskRepository.save();
        List<Task> taskList = project.getTasksBacklog();
        taskList.remove(task);
        project.setTasksBacklog(taskList);
        project.setModified(LocalDateTime.now());
        projectRepository.update(project);
        return task;
    }

    @Override
    public long count() {
        return taskRepository.count();
    }

    @Override
    public StatusTask updateStatusTask(User loggedUser, Task task, StatusTask status) throws UnauthorisedOperationException, NonexistingEntityException {
        if (!loggedUser.getRoles().contains(Role.ADMIN) && !loggedUser.equals(task.getProject().getOwner()) &&
                !task.getDevelopersAssigned().contains(loggedUser)) {
            throw new UnauthorisedOperationException("Only an administrator, the project owner or a developer from the task assigned team may update tasks.");
        }
        task.setStatusTask(status);
        task.setModified(LocalDateTime.now());
        taskRepository.update(task);
        return status;
    }

    @Override
    public Collection<Task> searchTask(String searchString) {
        String searchWord = searchString.toLowerCase();
        return getAll().stream().filter(task -> task.getTitle().toLowerCase().contains(searchWord)
                || Arrays.stream(task.getTags().split(",")).anyMatch(t -> t.equalsIgnoreCase(searchWord)))
                .collect(Collectors.toList());
    }

    private void validateTask(Task task) throws InvalidEntityDataException {
        int titleLength = task.getTitle().length();
        if (titleLength < TITLE_MIN_LENGTH || titleLength > TITLE_MAX_LENGTH) {
            throw new InvalidEntityDataException(String.format("Title length must be between %d and %d characters.",
                    TITLE_MIN_LENGTH, TITLE_MAX_LENGTH));
        }
        if (task.getEstimatedEffort() < 0) {
            throw new InvalidEntityDataException("Estimated effort should be positive integer value.");
        }
        if (task.getDescription() != null) {
            int descriptionLength = task.getDescription().length();
            if (descriptionLength < DESCRIPTION_MIN_LENGTH || descriptionLength > DESCRIPTION_MAX_LENGTH) {
                throw new InvalidEntityDataException(String.format("Task description must be between %d and %d characters.",
                        DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH));
            }
        }
    }

}
