package course.academy.service.impl;

import course.academy.dao.*;
import course.academy.service.*;

public class ServiceFactoryImpl implements ServiceFactory {
    @Override
    public UserService createUserService(UserRepository userRepository) {
        return new UserServiceImpl(userRepository);
    }

    @Override
    public TaskService createTaskService(TaskRepository taskRepository, ProjectRepository projectRepository) {
        return new TaskServiceImpl(taskRepository, projectRepository);
    }

    @Override
    public TaskResultService createTaskResultService(TaskResultRepository taskResultRepository) {
        return new TaskResultServiceImpl(taskResultRepository);
    }

    @Override
    public SprintService createSprintService(SprintRepository sprintRepository, TaskRepository taskRepository, UserRepository userRepository) {
        return new SprintServiceImpl(sprintRepository, taskRepository, userRepository);
    }

    @Override
    public SprintResultService createSprintResultService(SprintResultRepository sprintResultRepository) {
        return new SprintResultServiceImpl(sprintResultRepository);
    }

    @Override
    public ProjectService createProjectService(ProjectRepository projectRepository, UserRepository userRepository) {
        return new ProjectServiceImpl(projectRepository, userRepository);
    }

    @Override
    public ProjectResultService createProjectResultService(ProjectResultRepository projectResultRepository) {
        return new ProjectResultServiceImpl(projectResultRepository);
    }


}
