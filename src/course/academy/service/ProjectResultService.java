package course.academy.service;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.ProjectResult;
import course.academy.model.User;

import java.util.Collection;

public interface ProjectResultService {
    Collection<ProjectResult> getAll();
    ProjectResult getById(long id) throws NonexistingEntityException;
    ProjectResult add(User loggedUser, ProjectResult projectResult) throws InvalidEntityDataException, UnauthorisedOperationException;
    ProjectResult update(User loggedUser, ProjectResult projectResult) throws NonexistingEntityException, InvalidEntityDataException, UnauthorisedOperationException;
    ProjectResult deleteById(User loggedUser, long id) throws NonexistingEntityException, UnauthorisedOperationException;
    long count();
}
