package course.academy.service;


import course.academy.dao.*;

public interface ServiceFactory {

    UserService createUserService(UserRepository userRepository);
    TaskService createTaskService(TaskRepository taskRepository, ProjectRepository projectRepository);
    TaskResultService createTaskResultService(TaskResultRepository taskResultRepository);
    SprintService createSprintService(SprintRepository sprintRepository, TaskRepository taskRepository, UserRepository userRepository);
    SprintResultService createSprintResultService(SprintResultRepository sprintResultRepository);
    ProjectService createProjectService(ProjectRepository projectRepository, UserRepository userRepository);
    ProjectResultService createProjectResultService(ProjectResultRepository projectResultRepository);

}
