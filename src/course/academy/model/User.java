package course.academy.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static course.academy.utils.ParsingHelpers.dtf;

public class User implements Identifiable<Long>, Serializable {
    public static String checkUsername = "^[\\w]{2,15}$";
    public static String checkPassword = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#?!@$%^&*-])[A-Za-z\\d#?!@$%^&*-]{8,15}$";
    public static String checkEmail = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
    public static final int PASSWORD_MIN_LENGTH = 8;
    public static final int PASSWORD_MAX_LENGTH = 15;
    public static final int FIRST_NAME_MIN_LENGTH = 2;
    public static final int FIRST_NAME_MAX_LENGTH = 15;
    public static final int LAST_NAME_MIN_LENGTH = 2;
    public static final int LAST_NAME_MAX_LENGTH = 15;
    public static final int USERNAME_MIN_LENGTH = 2;
    public static final int USERNAME_MAX_LENGTH = 15;
    public static final int CONTACTS_MIN_LENGTH = 10;
    public static final int CONTACTS_MAX_LENGTH = 250;

    public List<Task> assignedTask = new ArrayList<>();
    public List<TaskResult> completedTaskResults = new ArrayList<>();
    public List<Project> projects = new ArrayList<>();
    public List<ProjectResult> completedProjectResult = new ArrayList<>();

    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private Set<Role> roles = new HashSet<>();
    private String contacts;
    private StatusUser statusUser = StatusUser.CHANGE_PASSWORD;
    private final LocalDateTime created = LocalDateTime.now();
    private LocalDateTime modified = LocalDateTime.now();


    public User() {
        roles.add(Role.DEVELOPER);
        setRoles(roles);
    }

    public User(String firstName, String lastName, String email, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
        roles.add(Role.DEVELOPER);
        setRoles(roles);
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    public StatusUser getStatusUser() {
        return statusUser;
    }

    public void setStatusUser(StatusUser statusUser) {
        this.statusUser = statusUser;
    }

    public String getPassword() {
        return password;
    }

    public List<Task> getAssignedTask() {
        return assignedTask;
    }

    public void setAssignedTask(List<Task> assignedTask) {
        this.assignedTask = assignedTask;
    }

    public List<TaskResult> getCompletedTaskResults() {
        return completedTaskResults;
    }

    public void setCompletedTaskResults(List<TaskResult> completedTaskResults) {
        this.completedTaskResults = completedTaskResults;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<ProjectResult> getCompletedProjectResult() {
        return completedProjectResult;
    }

    public void setCompletedProjectResult(List<ProjectResult> completedProjectResult) {
        this.completedProjectResult = completedProjectResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        return getId() != null ? getId().equals(user.getId()) : user.getId() == null;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("User {");
        sb.append("id=").append(id);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", roles=").append(roles).append(",");
        sb.append(System.lineSeparator()).append("currently assigned tasks: ");
        if (getAssignedTask().isEmpty()) {
            sb.append("no tasks, ");
        } else {
            for (Task task : getAssignedTask()) {
                sb.append("{id =").append(task.getId());
                sb.append(", title='").append(task.getTitle()).append("'}, ");
            }
        }
        sb.append(System.lineSeparator()).append("created=").append(created.format(dtf));
        sb.append(", modified=").append(modified.format(dtf));
        sb.append('}');
        return sb.toString();
    }

    }










