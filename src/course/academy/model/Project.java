package course.academy.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static course.academy.utils.ParsingHelpers.dtf;
import static course.academy.utils.ParsingHelpers.df;

public class Project implements Identifiable<Long>, Serializable {
    public static final int TITLE_MIN_LENGTH = 2;
    public static final int TITLE_MAX_LENGTH = 120;
    public static final int DESCRIPTION_MIN_LENGTH = 10;
    public static final int DESCRIPTION_MAX_LENGTH = 2500;

    private long id;
    private String title;
    private LocalDate startDate;
    private String description;
    private User owner;
    private List<User> developers = new ArrayList<>();
    private Sprint currentSprint;
    private List<SprintResult> previousSprintResults = new ArrayList<>();
    private List<Task> tasksBacklog = new ArrayList<>();
    private String tags;
    private final LocalDateTime created = LocalDateTime.now();
    private LocalDateTime modified = LocalDateTime.now();


    public Project() {
    }

    public Project(String title, LocalDate startDate, User owner, String tags) {
        this.title = title;
        this.startDate = startDate;
        this.owner = owner;
        this.tags = tags;
    }

    public Project(String title, LocalDate startDate, String description, User owner, String tags) {
        this(title,startDate,owner,tags);
        this.description = description;
    }


    public Project(String title, LocalDate startDate, User owner,
                   List<User> developers, List<Task> tasksBacklog, String tags) {
        this(title,startDate,owner,tags);
        this.developers = developers;
        this.tasksBacklog = tasksBacklog;
    }

    public Project(String title, LocalDate startDate, String description, User owner,
                   List<User> developers, List<Task> tasksBacklog, String tags) {
        this(title,startDate,owner,tags);
        this.developers = developers;
        this.tasksBacklog = tasksBacklog;
        this.description = description;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<User> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<User> developers) {
        this.developers = developers;
    }

    public Sprint getCurrentSprint() {
        return currentSprint;
    }

    public void setCurrentSprint(Sprint currentSprint) {
        this.currentSprint = currentSprint;
    }

    public List<SprintResult> getPreviousSprintResults() {
        return previousSprintResults;
    }

    public void setPreviousSprintResults(List<SprintResult> previousSprintResults) {
        this.previousSprintResults = previousSprintResults;
    }

    public List<Task> getTasksBacklog() {
        return tasksBacklog;
    }

    public void setTasksBacklog(List<Task> tasksBacklog) {
        this.tasksBacklog = tasksBacklog;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Project)) return false;

        Project project = (Project) o;

        return id == project.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("Project {");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", startDate=").append(startDate.format(df));
        sb.append(", owner= {id=").append(owner.getId()).append(", username=").append(owner.getUsername()).append("},");
        sb.append(System.lineSeparator()).append("developers assigned: ");
        if (developers.isEmpty()) {
            sb.append("no developers assigned yet,");
        } else {
            for (User developer : developers) {
                sb.append("{id=").append(developer.getId()).append(", username=").append(developer.getUsername()).append("}, ");
            }
        }
        sb.append(System.lineSeparator()).append("current/last sprint: ");
        if (currentSprint != null) {
            sb.append("{id=").append(currentSprint.getId()).append(", start date=").append(startDate.format(df)).append("}, ");
        } else {
            sb.append("no sprint started yet,");
        }
        sb.append(System.lineSeparator()).append("previous sprint results: ");
        if (previousSprintResults.isEmpty()) {
            sb.append("no previous sprints available,");
            } else {
            for (SprintResult result:previousSprintResults) {
                sb.append("{id=").append(result.getId())
                        .append(", actual effort=").append(result.getTeamVelocity()).append(" effort units per day},");
            }
        }
        sb.append(System.lineSeparator()).append("tasks backlog: ");
        if (tasksBacklog.isEmpty()) {
            sb.append(" no tasks assigned to the project yet,");
        } else {
            for (Task task : tasksBacklog) {
                sb.append("{id =").append(task.getId());
                sb.append(", title='").append(task.getTitle()).append("'}, ");
            }
        }
        sb.append(System.lineSeparator()).append("created=").append(created.format(dtf));
        sb.append(", modified=").append(modified.format(dtf));
        sb.append('}');
        return sb.toString();
    }
}





