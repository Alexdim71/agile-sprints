package course.academy.model.mapper;

import course.academy.exception.NonexistingEntityException;
import course.academy.model.Project;
import course.academy.model.Task;
import course.academy.model.User;
import course.academy.model.dto.ProjectDto;
import course.academy.service.ProjectService;
import course.academy.service.TaskService;
import course.academy.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class ProjectMapper {
    private UserService userService;
    private ProjectService projectService;
    private TaskService taskService;

    public ProjectMapper(UserService userService, ProjectService projectService, TaskService taskService) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    public Project fromDto(ProjectDto dto) throws NonexistingEntityException {
        Project project = new Project();
        dtoToObject(dto, project);
        return project;
    }

    public Project fromDto(ProjectDto dto, long id) throws NonexistingEntityException {
        Project project = projectService.getById(id);
        dtoToObject(dto, project);
        return project;
    }


    private void dtoToObject(ProjectDto dto, Project project) throws NonexistingEntityException {
        User owner = userService.getById(dto.getOwnerId());
        project.setOwner(owner);
        project.setTitle(dto.getTitle());
        project.setStartDate(dto.getStartDate());
        project.setTags(dto.getTags());
        project.setDescription(dto.getDescription());
        List<User> developers = new ArrayList<>();
        if (dto.getDeveloperIds() != null) {
            for (int id : dto.getDeveloperIds()) {
                developers.add(userService.getById(id));
            }
        }
        project.setDevelopers(developers);
        List<Task> tasks = new ArrayList<>();
        if (dto.getTasksIds() != null) {
            for (int id : dto.getTasksIds()) {
                tasks.add(taskService.getById(id));
            }
        }
        project.setTasksBacklog(tasks);
    }

}
