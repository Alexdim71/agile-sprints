package course.academy.model.mapper;

import course.academy.exception.NonexistingEntityException;

import course.academy.model.User;
import course.academy.model.dto.UserDto;

import course.academy.model.dto.UserManagementDto;
import course.academy.service.UserService;

public class UserMapper {
    private UserService userService;

    public UserMapper(UserService userService) {
        this.userService = userService;
    }

    public UserDto userToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setContacts(user.getContacts());
        return userDto;
    }

    public UserManagementDto userToManagementDto(User user) {
        UserManagementDto userDto = new UserManagementDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setContacts(user.getContacts());
        userDto.setRoles(user.getRoles());
        userDto.setStatusUser(user.getStatusUser());
        return userDto;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(UserDto userDto, long id) throws NonexistingEntityException {
        User user = userService.getById(id);
        dtoToObject(userDto, user);
        return user;
    }

    public User fromManagementDto(UserManagementDto userManagementDto, long id) throws NonexistingEntityException {
        User user = userService.getById(id);
        managementDtoToObject(userManagementDto, user);
        return user;
    }

    private void dtoToObject(UserDto userDto, User user) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setContacts(userDto.getContacts());
        user.setUsername(user.getUsername());
        user.setPassword(user.getPassword());
        user.setRoles(user.getRoles());
        user.setStatusUser(user.getStatusUser());
    }

    private void managementDtoToObject(UserManagementDto userManagementDto, User user) {
        user.setFirstName(userManagementDto.getFirstName());
        user.setLastName(userManagementDto.getLastName());
        user.setEmail(userManagementDto.getEmail());
        user.setContacts(userManagementDto.getContacts());
        user.setRoles(userManagementDto.getRoles());
        user.setStatusUser(userManagementDto.getStatusUser());
        user.setUsername(user.getUsername());
        user.setPassword(user.getPassword());
    }



}
