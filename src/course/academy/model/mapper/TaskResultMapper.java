package course.academy.model.mapper;

import course.academy.exception.NonexistingEntityException;
import course.academy.model.Task;
import course.academy.model.TaskResult;
import course.academy.model.dto.TaskResultDto;
import course.academy.service.TaskResultService;
import course.academy.service.TaskService;


public class TaskResultMapper {
    private TaskService taskService;
    private TaskResultService taskResultService;

    public TaskResultMapper(TaskService taskService, TaskResultService taskResultService) {
        this.taskService = taskService;
        this.taskResultService = taskResultService;
    }

    public TaskResult fromDto(TaskResultDto dto) throws NonexistingEntityException {
        TaskResult taskResult = new TaskResult();
        dtoToObject(dto, taskResult);
        return taskResult;
    }

    public TaskResult fromDto(TaskResultDto dto, long id) throws NonexistingEntityException {
        TaskResult taskResult = taskResultService.getById(id);
        dtoToObject(dto, taskResult);
        return taskResult;
    }




    private void dtoToObject(TaskResultDto dto, TaskResult taskResult) throws NonexistingEntityException {
       Task task = taskService.getById(dto.getTaskId());
       taskResult.setTask(task);
       taskResult.setResultDescription(dto.getResultDescription());
    };


}
