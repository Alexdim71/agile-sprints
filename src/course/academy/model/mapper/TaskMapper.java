package course.academy.model.mapper;

import course.academy.exception.NonexistingEntityException;
import course.academy.model.Project;
import course.academy.model.Task;
import course.academy.model.dto.TaskDto;
import course.academy.service.ProjectService;
import course.academy.service.TaskService;


public class TaskMapper {
    private TaskService taskService;
    private ProjectService projectService;

    public TaskMapper(TaskService taskService, ProjectService projectService) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    public Task fromDto(TaskDto dto) throws NonexistingEntityException {
        Task task = new Task();
        dtoToObject(dto, task);
        return task;
    }

    public Task fromDto(TaskDto dto, long id) throws NonexistingEntityException {
        Task task = taskService.getById(id);
        dtoToObject(dto, task);
        return task;
    }


    private void dtoToObject(TaskDto dto, Task task) throws NonexistingEntityException {
        Project project = projectService.getById(dto.getProjectId());
        task.setProject(project);
        task.setKind(dto.getKind());
        task.setTitle(dto.getTitle());
        task.setEstimatedEffort(dto.getEstimatedEffort());
        task.setTags(dto.getTags());
        task.setDescription(dto.getDescription());
    };


}
