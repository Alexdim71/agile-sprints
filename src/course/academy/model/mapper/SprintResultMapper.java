package course.academy.model.mapper;

import course.academy.exception.NonexistingEntityException;
import course.academy.model.Sprint;
import course.academy.model.SprintResult;
import course.academy.model.dto.SprintResultDto;
import course.academy.service.SprintResultService;
import course.academy.service.SprintService;


public class SprintResultMapper {
    private SprintResultService sprintResultService;
    private SprintService sprintService;

    public SprintResultMapper(SprintResultService sprintResultService, SprintService sprintService) {
        this.sprintResultService = sprintResultService;
        this.sprintService = sprintService;
    }

    public SprintResult fromDto(SprintResultDto dto) throws NonexistingEntityException {
        SprintResult sprintResult = new SprintResult();
        dtoToObject(dto, sprintResult);
        return sprintResult;
    }

    public SprintResult fromDto(SprintResultDto dto, long id) throws NonexistingEntityException {
        SprintResult sprintResult = sprintResultService.getById(id);
        dtoToObject(dto, sprintResult);
        return sprintResult;
    }

    private void dtoToObject(SprintResultDto dto, SprintResult sprintResult) throws NonexistingEntityException {
        Sprint sprint = sprintService.getById(dto.getSprintId());
        sprintResult.setSprint(sprint);
        sprintResult.setResultsDescription(dto.getResultsDescription());
    }

}
