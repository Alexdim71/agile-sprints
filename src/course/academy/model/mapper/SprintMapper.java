package course.academy.model.mapper;

import course.academy.exception.NonexistingEntityException;
import course.academy.model.Project;
import course.academy.model.Sprint;
import course.academy.model.User;
import course.academy.model.dto.SprintDto;
import course.academy.service.ProjectService;
import course.academy.service.SprintService;
import course.academy.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class SprintMapper {
    private UserService userService;
    private ProjectService projectService;
    private SprintService sprintService;

    public SprintMapper(UserService userService, ProjectService projectService, SprintService sprintService) {
        this.userService = userService;
        this.projectService = projectService;
        this.sprintService = sprintService;
    }

    public Sprint fromDto(SprintDto dto) throws NonexistingEntityException {
        Sprint sprint = new Sprint();
        dtoToObject(dto, sprint);
        return sprint;
    }

    public Sprint fromDto(SprintDto dto, long id) throws NonexistingEntityException {
        Sprint sprint = sprintService.getById(id);
        dtoToObject(dto, sprint);
        return sprint;
    }

    private void dtoToObject(SprintDto dto, Sprint sprint) throws NonexistingEntityException {
        Project project = projectService.getById(dto.getProjectId());
        sprint.setProject(project);
        sprint.setStartDate(dto.getStartDate());
        sprint.setEndDate(dto.getStartDate().plusDays(dto.getDuration()-1));
        sprint.setDuration(dto.getDuration());
        List<User> developers = new ArrayList<>();
        if (dto.getDevelopersId() != null) {
            for (int id : dto.getDevelopersId()) {
                developers.add(userService.getById(id));
            }
        }
        sprint.setDevelopers(developers);
    }


}
