package course.academy.model;

public enum StatusTask {
    PLANNED, ACTIVE, COMPLETED
}
