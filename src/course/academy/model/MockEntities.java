package course.academy.model;

import course.academy.utils.ParsingHelpers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MockEntities {

    public static List<User> mockUsers = new ArrayList<>();
    public static List<User> sprint1Developers = new ArrayList<>();
    public static List<User> sprint2Developers = new ArrayList<>();
    public static List<Task> mockTasks = new ArrayList<>();


    public static User admin1 = new User("Admin", "Admin", "admin@abv.bg",
            "administrator1" , "mockPassword1$");
    public static User user2Owner = new User("Alexander", "Dimov", "a_dimov@yahoo.com", "alex_dimov", "mockPassword2$");
    public static User user3Developer = new User("John", "Doe", "john_doe@yahoo.com", "john_doe", "mockPassword3$");
    public static User user4Developer = new User("Peter", "Brown", "peter_brown@yahoo.com", "peter_brown", "mockPassword3$");
    public static User user5Developer = new User("Ivan", "Ivanov", "ivan_inavov@yahoo.com", "ivan_ivanov", "mockPassword3$");
    public static User user6Developer = new User("Peter", "Petrov", "peter_petrov@yahoo.com", "peter_petrov", "mockPassword3$");
    public static User user7Owner = new User("Todor", "Todorov", "todor_todorov@yahoo.com", "todor_todorov", "mockPassword3$");
    public static User user8Test = new User("Peter", "Petrov", "peter_petrov@yahoo.com", "peter_petrov2", "mockPassword3$");

    LocalDate date = LocalDate.now();

    public static Project project1 = new Project("Mock Project", LocalDate.parse("24.03.2022", ParsingHelpers.df), user2Owner,
            "project, mock, agile");

    public static Task task1 = new Task(Kind.IMPLEMENTATION, project1, "Phase 1 design",16,
            "mock, design");
    public static Task task2 = new Task(Kind.IMPLEMENTATION, project1, "Phase 1 implement",88, "the first implementation task to test description field",
             "mock, implement");
    public static Task task2Addition = new Task(Kind.IMPLEMENTATION, project1, "Phase 1 implement addition", 16,
            "mock, implement, addition" );
    public static Task task3 = new Task(Kind.IMPLEMENTATION, project1, "Phase 1 tests",16,
            "mock, test" );
    public static Task task4 = new Task(Kind.IMPLEMENTATION, project1, "Phase 2 design",16,
             "mock, design");
    public static Task task5 = new Task(Kind.IMPLEMENTATION, project1, "Phase 2 implement",88,
             "mock, implement");
    public static Task task5Addition = new Task(Kind.IMPLEMENTATION, project1, "Phase 2 implement addition", 16,
            "mock, implement");
    public static Task task6 = new Task(Kind.IMPLEMENTATION, project1, "Phase 2 tests",16,
             "mock, test");


    public static Sprint sprint1 = new Sprint(project1, LocalDate.parse("26.03.2022", ParsingHelpers.df), 8,
            sprint1Developers);
    public static Sprint sprint2 = new Sprint(project1, LocalDate.parse("03.04.2022", ParsingHelpers.df), 8,
            sprint2Developers);

    public static ProjectResult projectResult1 = new ProjectResult(project1,LocalDate.parse("10.04.2022",ParsingHelpers.df));

    public static TaskResult task1Result = new TaskResult(task1,16);
    public static TaskResult task2Result = new TaskResult(task2,72);
    public static TaskResult task2AdditionResult = new TaskResult(task2Addition,16);
    public static TaskResult task3Result = new TaskResult(task3,16);

    public static SprintResult sprint1Result = new SprintResult(sprint1);

    public static TaskResult task4Result = new TaskResult(task4,16);
    public static TaskResult task5Result = new TaskResult(task5,72);
    public static TaskResult task5AdditionResult = new TaskResult(task5Addition,16);
    public static TaskResult task6Result = new TaskResult(task6,16);

    public static SprintResult sprint2Result = new SprintResult(sprint2,
            "Sprint was successful, one new feature was added");


    static {
        mockUsers = List.of(admin1, user2Owner, user3Developer, user4Developer, user5Developer, user6Developer, user7Owner);
        mockTasks = List.of(task1, task2, task2Addition, task3, task4, task5, task5Addition, task6);
        sprint1Developers.add(user3Developer);
        sprint1Developers.add(user4Developer);
        sprint1Developers.add(user5Developer);
        sprint2Developers.add(user3Developer);
        sprint2Developers.add(user4Developer);
        sprint2Developers.add(user5Developer);
    }
}
