package course.academy.model.dto;

import course.academy.model.Kind;

public class TaskDto {

    private long projectId;
    private Kind kind;
    private String title;
    private int estimatedEffort;
    private String tags;
    private String description;

    public TaskDto() {
    }

    public TaskDto(long projectId, Kind kind, String title, int estimatedEffort, String tags) {
        this.projectId = projectId;
        this.kind = kind;
        this.title = title;
        this.estimatedEffort = estimatedEffort;
        this.tags = tags;
    }

    public TaskDto(int projectId, Kind kind, String title, int estimatedEffort, String tags, String description) {

        this.projectId = projectId;
        this.kind = kind;
        this.title = title;
        this.estimatedEffort = estimatedEffort;
        this.tags = tags;
        this.description = description;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getEstimatedEffort() {
        return estimatedEffort;
    }

    public void setEstimatedEffort(int estimatedEffort) {
        this.estimatedEffort = estimatedEffort;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}