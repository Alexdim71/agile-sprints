package course.academy.model.dto;

import java.time.LocalDate;

public class ProjectResultDto {

    private long projectId;
    private LocalDate endDate;
    private String resultsDescription;

    public ProjectResultDto() {
    }

    public ProjectResultDto(long projectId, LocalDate endDate) {
        this.projectId = projectId;
        this.endDate = endDate;
    }

    public ProjectResultDto(long projectId, LocalDate endDate, String resultsDescription) {
        this.projectId = projectId;
        this.endDate = endDate;
        this.resultsDescription = resultsDescription;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getResultsDescription() {
        return resultsDescription;
    }

    public void setResultsDescription(String resultsDescription) {
        this.resultsDescription = resultsDescription;
    }
}
