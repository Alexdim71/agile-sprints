package course.academy.model.dto;

public class TaskResultDto {

    private long taskId;
    private int actualEffort;
    private String resultDescription;

    public TaskResultDto() {
    }

    public TaskResultDto(long taskId, int actualEffort) {
        this.taskId = taskId;
        this.actualEffort = actualEffort;
    }

    public TaskResultDto(long taskId, int actualEffort, String resultDescription) {
        this.taskId = taskId;
        this.actualEffort = actualEffort;
        this.resultDescription = resultDescription;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public int getActualEffort() {
        return actualEffort;
    }

    public void setActualEffort(int actualEffort) {
        this.actualEffort = actualEffort;
    }

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }
}