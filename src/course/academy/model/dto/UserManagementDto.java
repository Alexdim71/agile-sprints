package course.academy.model.dto;

import course.academy.model.Role;
import course.academy.model.StatusUser;

import java.util.Set;

public class UserManagementDto {

    private String firstName;
    private String lastName;
    private String email;
    private String contacts;
    private Set<Role> roles;
    private StatusUser statusUser;


    public UserManagementDto() {
    }

    public UserManagementDto(String firstName, String lastName, String email,
                             String contacts, Set<Role> roles, StatusUser statusUser) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contacts = contacts;
        this.roles = roles;
        this.statusUser = statusUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public StatusUser getStatusUser() {
        return statusUser;
    }

    public void setStatusUser(StatusUser statusUser) {
        this.statusUser = statusUser;
    }
}









