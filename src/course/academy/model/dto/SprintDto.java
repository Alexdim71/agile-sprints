package course.academy.model.dto;

import java.time.LocalDate;
import java.util.List;

public class SprintDto {

    private long projectId;
    private LocalDate startDate;
    private int duration;

    private List<Integer> developersId;

    public SprintDto() {
    }

    public SprintDto(long projectId, LocalDate startDate, int duration) {
        this.projectId = projectId;
        this.startDate = startDate;
        this.duration = duration;
    }

    public SprintDto(long projectId, LocalDate startDate, int duration, List<Integer> developersId) {
        this.projectId = projectId;
        this.startDate = startDate;
        this.duration = duration;
        this.developersId = developersId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<Integer> getDevelopersId() {
        return developersId;
    }

    public void setDevelopersId(List<Integer> developersId) {
        this.developersId = developersId;
    }
}
