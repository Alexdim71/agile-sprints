package course.academy.model.dto;

import java.time.LocalDate;
import java.util.List;

public class ProjectDto {

    private String title;
    private LocalDate startDate;
    private String description;
    private long ownerId;
    private String tags;

    private List<Integer> developerIds;
    private List<Integer> tasksIds;

    public ProjectDto() {
    }

//    public ProjectDto(String title, LocalDate startDate, long ownerId, String tags) {
//        this.title = title;
//        this.startDate = startDate;
//        this.ownerId = ownerId;
//        this.tags = tags;
//    }
//
//    public ProjectDto(String title, LocalDate startDate, String description, long ownerId, String tags) {
//        this.title = title;
//        this.startDate = startDate;
//        this.description = description;
//        this.ownerId = ownerId;
//        this.tags = tags;
//    }
//
//    public ProjectDto(String title, LocalDate startDate, long ownerId, String tags, List<Integer> developerIds) {
//        this.title = title;
//        this.startDate = startDate;
//        this.ownerId = ownerId;
//        this.tags = tags;
//        this.developerIds = developerIds;
//    }
//
//    public ProjectDto(String title, LocalDate startDate, long ownerId, String tags, List<Integer> developerIds, List<Integer> tasksIds) {
//        this.title = title;
//        this.startDate = startDate;
//        this.ownerId = ownerId;
//        this.tags = tags;
//        this.developerIds = developerIds;
//        this.tasksIds = tasksIds;
//    }

    public ProjectDto(String title, LocalDate startDate, String description, long ownerId, String tags,
                      List<Integer> developerIds, List<Integer> tasksIds) {
        this.title = title;
        this.startDate = startDate;
        this.description = description;
        this.ownerId = ownerId;
        this.tags = tags;
        this.developerIds = developerIds;
        this.tasksIds = tasksIds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public List<Integer> getDeveloperIds() {
        return developerIds;
    }

    public void setDeveloperIds(List<Integer> developerIds) {
        this.developerIds = developerIds;
    }

    public List<Integer> getTasksIds() {
        return tasksIds;
    }

    public void setTasksIds(List<Integer> tasksIds) {
        this.tasksIds = tasksIds;
    }
}
