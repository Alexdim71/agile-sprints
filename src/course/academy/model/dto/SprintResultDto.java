package course.academy.model.dto;

import java.time.LocalDate;

public class SprintResultDto {

    private long sprintId;
    private String resultsDescription;


    public SprintResultDto() {
    }

    public SprintResultDto(long sprintId) {
        this.sprintId = sprintId;
    }

    public SprintResultDto(long sprintId, String resultsDescription) {
        this.sprintId = sprintId;
        this.resultsDescription = resultsDescription;
    }

    public long getSprintId() {
        return sprintId;
    }

    public void setSprintId(long sprintId) {
        this.sprintId = sprintId;
    }

    public String getResultsDescription() {
        return resultsDescription;
    }

    public void setResultsDescription(String resultsDescription) {
        this.resultsDescription = resultsDescription;
    }
}
