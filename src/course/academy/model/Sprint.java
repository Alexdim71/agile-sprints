package course.academy.model;

import course.academy.utils.ParsingHelpers;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static course.academy.utils.ParsingHelpers.dtf;
import static course.academy.utils.ParsingHelpers.df;

public class Sprint implements Identifiable<Long>, Serializable {

    private long id;
    private Project project; //added additionally to the template
    private LocalDate startDate;
    private int duration;
    private LocalDate endDate;
    private User owner;
    private List<User> developers = new ArrayList<>();
    private List<Task> tasks = new ArrayList<>();
    private List<TaskResult> completedTaskResults = new ArrayList<>();
    private final LocalDateTime created = LocalDateTime.now();
    private LocalDateTime modified = LocalDateTime.now();

    public Sprint() {
    }

    public Sprint(Project project, LocalDate startDate, int duration) {
        this.project = project;
        this.startDate = startDate;
        this.duration = duration;
        this.endDate = startDate.plusDays(duration-1);
    }

    public Sprint(Project project, LocalDate startDate, int duration, List<User> developers) {
        this.project = project;
        this.startDate = startDate;
        this.duration = duration;
        this.developers = developers;
        this.endDate = startDate.plusDays(duration-1);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<User> getDevelopers() {
        return new ArrayList<>(developers);
    }

    public void setDevelopers(List<User> developers) {
        this.developers = developers;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<TaskResult> getCompletedTaskResults() {
        return completedTaskResults;
    }

    public void setCompletedTaskResults(List<TaskResult> completedTaskResults) {
        this.completedTaskResults = completedTaskResults;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sprint)) return false;

        Sprint sprint = (Sprint) o;

        return id == sprint.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder("Sprint {");
        sb.append("id=").append(id);
        sb.append(", project={id=").append(project.getId()).append(", title=").append(project.getTitle()).append("}");
        sb.append(", startDate=").append(startDate.format(df));
        sb.append(", endDate=").append(endDate.format(df));
        sb.append(", duration=").append(duration).append(" days");
        sb.append(", owner={id=").append(owner.getId());
        sb.append(", username =").append(owner.getUsername()).append("}, ");
        sb.append(System.lineSeparator()).append("developers: ");
        if (!developers.isEmpty()) {
            for (User developer : developers) {
                sb.append("{id=").append(developer.getId());
                sb.append(", username=").append(developer.getUsername()).append("}, ");
            }
        }
        sb.append(System.lineSeparator()).append("tasks: ");
        for (Task task: tasks) {
            sb.append("{id=").append(task.getId());
            sb.append(", title='").append(task.getTitle()).append("'}, ");
        }
        sb.append(System.lineSeparator()).append("completed task results: ");
        if (completedTaskResults.isEmpty()) {
            sb.append("No completed tasks results available yet, ");
        } else {
            for (TaskResult result : completedTaskResults) {
                sb.append("{id =").append(result.getId());
                sb.append(", verified by={id=").append(result.getVerifiedBy().getId())
                        .append(", actual effort =").append(result.getActualEffort()).append(" effort units")
                        .append(", username=").append(result.getVerifiedBy().getUsername()).append("}, ");
            }
        }
        sb.append(System.lineSeparator()).append("created=").append(created.format(dtf)).append(", modified=").append(modified.format(dtf));
        sb.append('}');

        return sb.toString();
    }
}
