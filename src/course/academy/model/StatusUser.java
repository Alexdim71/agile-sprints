package course.academy.model;

public enum StatusUser {
    ACTIVE, CHANGE_PASSWORD, SUSPENDED, DEACTIVATED
}
