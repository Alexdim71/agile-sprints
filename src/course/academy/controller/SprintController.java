package course.academy.controller;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Sprint;
import course.academy.model.Task;
import course.academy.model.User;
import course.academy.model.mapper.SprintMapper;
import course.academy.service.SprintService;
import course.academy.service.TaskService;
import course.academy.service.UserService;
import course.academy.view.Menu;
import course.academy.view.SprintDialog;

import java.util.List;

public class SprintController {


    private SprintService sprintService;
    private SprintMapper sprintMapper;
    private UserService userService;
    private TaskService taskService;

    public SprintController(SprintService sprintService, SprintMapper sprintMapper,
                            UserService userService, TaskService taskService) {
        this.sprintService = sprintService;
        this.sprintMapper = sprintMapper;
        this.userService = userService;
        this.taskService = taskService;
    }

    public void init(User loggedUser) {
        var menu = new Menu("Sprints CRUD Menu", List.of(
                new Menu.Option("Print All Sprints", () -> {
                    var sprints = sprintService.getAll();
                    sprints.forEach(System.out::println);
                    return "Total sprint count: " + sprints.size();
                }),

                new Menu.Option("Enter New Sprint", () -> {
                    var dto = new SprintDialog().input();
                    Sprint sprint;
                    try {
                        sprint = sprintMapper.fromDto(dto);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Sprint to project with ID:%d was not added.", dto.getProjectId());
                    }
                    Sprint created;
                    try {
                        created = sprintService.add(loggedUser, sprint);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return String.format("Sprint to project with ID:%d was not added.", dto.getProjectId());
                    }

                    return String.format("Sprint ID:%d to project with ID:%d was added successfully.",
                            created.getId(), created.getProject().getId());
                }),

                new Menu.Option("Update Existing Sprint", () -> {
                    var id = new SprintDialog().update();
                    Sprint toBeUpdated;
                    try {
                        toBeUpdated = sprintService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    var dto = new SprintDialog().input();
                    try {
                        toBeUpdated = sprintMapper.fromDto(dto, id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Sprint with ID:%d was not updated.", id);
                    }
                    Sprint updated;
                    try {
                        updated = sprintService.update(loggedUser, toBeUpdated);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Sprint with ID:%d was not updated.", id);
                    }
                    return String.format("Sprint ID:%d to project with title '%s'; was updated successfully.",
                            updated.getId(), updated.getProject().getTitle());
                }),

                new Menu.Option("Delete Existing Sprint", () -> {
                    var id = new SprintDialog().delete();
                    Sprint deleted;
                    try {
                        deleted = sprintService.deleteById(loggedUser, id);
                    } catch (NonexistingEntityException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("Sprint ID:%d to project with title '%s' was deleted successfully.",
                            deleted.getId(), deleted.getProject().getTitle());
                }),

                new Menu.Option("Print Single Sprint by ID", () -> {
                    var id = new SprintDialog().enterSprint();
                    Sprint sprint;
                    try {
                        sprint = sprintService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another sprint id.";
                    }
                    System.out.println(sprint);
                    return "";
                }),

                new Menu.Option("Add Task to Sprint", () -> {
                    var id = new SprintDialog().enterSprint();
                    Sprint sprint;
                    try {
                        sprint = sprintService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another sprint id.";
                    }
                    var taskId = new SprintDialog().addTaskMessage();

                    Task task;
                    try {
                        task = taskService.getById(taskId);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another task id.";
                    }

                    try {
                        sprintService.addTaskToSprint(loggedUser, sprint, task);
                    } catch (UnauthorisedOperationException | InvalidEntityDataException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("Task with title '%s' was added to Sprint ID:%d",
                            task.getTitle(), sprint.getId());
                }),

                new Menu.Option("Remove Task from Sprint", () -> {
                    var id = new SprintDialog().enterSprint();
                    Sprint sprint;
                    try {
                        sprint = sprintService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another sprint id.";
                    }
                    var taskId = new SprintDialog().removeTaskMessage();
                    Task task;
                    try {
                        task = taskService.getById(taskId);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another task id.";
                    }
                    try {
                        sprintService.removeTaskFromSprint(loggedUser, sprint, task);
                    } catch (UnauthorisedOperationException | InvalidEntityDataException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("Task with title '%s' was removed from Sprint ID:%d",
                            task.getTitle(), sprint.getId());
                }),

                new Menu.Option("Add Developer to Sprint", () -> {
                    var id = new SprintDialog().enterSprint();
                    Sprint sprint;
                    try {
                        sprint = sprintService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another sprint id.";
                    }
                    var userId = new SprintDialog().addDeveloperMessage();
                    User user;
                    try {
                        user = userService.getById(userId);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another user id.";
                    }
                    try {
                        sprintService.addDeveloperToSprint(loggedUser, sprint, user);
                    } catch (UnauthorisedOperationException | InvalidEntityDataException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("User with username '%s' was added to Sprint ID:%d",
                            user.getUsername(), sprint.getId());
                }),

                new Menu.Option("Remove Developer from Sprint", () -> {
                    var id = new SprintDialog().enterSprint();
                    Sprint sprint;
                    try {
                        sprint = sprintService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another sprint id.";
                    }
                    var userId = new SprintDialog().removeDeveloperMessage();
                    User user;
                    try {
                        user = userService.getById(userId);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another user id.";
                    }
                    try {
                        sprintService.removeDeveloperFromSprint(loggedUser, sprint, user);
                    } catch (UnauthorisedOperationException | InvalidEntityDataException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("User with username '%s' was removed from Sprint ID:%d",
                            user.getUsername(), sprint.getId());
                })

                ));

        menu.show();
    }


}
