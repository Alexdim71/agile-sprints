package course.academy.controller;

import course.academy.exception.InvalidEntityDataException;
import course.academy.model.User;
import course.academy.model.mapper.*;
import course.academy.service.*;
import course.academy.utils.MessageHelpers;
import course.academy.view.LoginDialog;
import course.academy.view.Menu;
import course.academy.view.UserRegisterDialog;

import java.util.List;
import java.util.Optional;


public class LoginController {

    private UserService userService;
    private UserMapper userMapper;
    private ProjectService projectService;
    private ProjectMapper projectMapper;
    private TaskService taskService;
    private TaskMapper taskMapper;
    private SprintService sprintService;
    private SprintMapper sprintMapper;
    private ProjectResultService projectResultService;
    private ProjectResultMapper projectResultMapper;
    private TaskResultService taskResultService;
    private TaskResultMapper taskResultMapper;
    private SprintResultService sprintResultService;
    private SprintResultMapper sprintResultMapper;

    public LoginController(UserService userService, UserMapper userMapper, ProjectService projectService,
                           ProjectMapper projectMapper, TaskService taskService, TaskMapper taskMapper,
                           SprintService sprintService, SprintMapper sprintMapper, ProjectResultService projectResultService,
                           ProjectResultMapper projectResultMapper, TaskResultService taskResultService,
                           TaskResultMapper taskResultMapper, SprintResultService sprintResultService,
                           SprintResultMapper sprintResultMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.projectService = projectService;
        this.projectMapper = projectMapper;
        this.taskService = taskService;
        this.taskMapper = taskMapper;
        this.sprintService = sprintService;
        this.sprintMapper = sprintMapper;
        this.projectResultService = projectResultService;
        this.projectResultMapper = projectResultMapper;
        this.taskResultService = taskResultService;
        this.taskResultMapper = taskResultMapper;
        this.sprintResultService = sprintResultService;
        this.sprintResultMapper = sprintResultMapper;
    }

    public void init() {
        var menu = new Menu("LOGIN MENU AGILE SPRINT MANAGEMENT SYSTEM", List.of(
                new Menu.Option("Register", () -> {
                    var user = new UserRegisterDialog().input();
                    User created;
                    try {
                        created = userService.add(user);
                    } catch (InvalidEntityDataException e) {
                        System.out.println(e.getMessage());
                        return String.format("User with username '%s' was not added.", user.getUsername());
                    }

                    return String.format("User ID:%d with username '%s' added successfully.",
                            created.getId(), created.getUsername());
                }),

                new Menu.Option("Login", () -> {
                    var username = new LoginDialog().inputUsername();
                    Optional<User> user = userService.findByUsername(username);
                    User loggedUser;
                    if (user.isEmpty()) {
                        return String.format("User with username '%s' does not exist.", username);
                    } else {
                        loggedUser = user.get();
                    }

                    var password = new LoginDialog().inputPassword();
                    if (!loggedUser.getPassword().equals(password)) {
                        loggedUser = null;
                        return String.format("Error: wrong password");
                    } else {
                        var mainController = new MainController(userService, userMapper, projectService, projectMapper,
                                taskService, taskMapper, sprintService, sprintMapper, projectResultService, projectResultMapper,
                                taskResultService, taskResultMapper, sprintResultService, sprintResultMapper);
                        mainController.init(loggedUser);
                        return String.format("User ID:%d with username '%s' logged successfully.",
                                loggedUser.getId(), loggedUser.getUsername());
                    }
                }),

                new Menu.Option("Application Description", () -> MessageHelpers.ABOUT)
        ));

        menu.show();

    }

    }


