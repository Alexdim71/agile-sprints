package course.academy.controller;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.ProjectResult;
import course.academy.model.User;
import course.academy.model.mapper.ProjectResultMapper;
import course.academy.service.ProjectResultService;
import course.academy.view.Menu;
import course.academy.view.ProjectResultDialog;

import java.util.List;

public class ProjectResultController {

    private ProjectResultService projectResultService;
    private ProjectResultMapper projectResultMapper;

    public ProjectResultController(ProjectResultService projectResultService, ProjectResultMapper projectResultMapper) {
        this.projectResultService = projectResultService;
        this.projectResultMapper = projectResultMapper;
    }

    public void init(User loggedUser) {
        var menu = new Menu("Project Results CRUD Menu", List.of(
                new Menu.Option("Print All Project Results", () -> {
                    var projectResults = projectResultService.getAll();
                    projectResults.forEach(System.out::println);
                    return "Total Project Results Count: " + projectResults.size();
                }),

                new Menu.Option("Enter New Project Result", () -> {
                    var dto = new ProjectResultDialog().input();
                    ProjectResult projectResult;
                    try {
                        projectResult = projectResultMapper.fromDto(dto);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project Result was not added.");
                    }
                    ProjectResult created;
                    try {
                        created = projectResultService.add(loggedUser, projectResult);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project Result was not added.");
                    }

                    return String.format("Project Result ID:%d to Project with title '%s' was added successfully.",
                            created.getId(), created.getProject().getTitle());
                }),

                new Menu.Option("Update Existing Project Result", () -> {
                    var id = new ProjectResultDialog().update();
                    ProjectResult toBeUpdated;
                    try {
                        toBeUpdated = projectResultService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    var dto = new ProjectResultDialog().input();
                    try {
                        toBeUpdated = projectResultMapper.fromDto(dto, id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project Result ID:%d was not updated.", id);
                    }
                    ProjectResult updated;
                    try {
                        updated = projectResultService.update(loggedUser, toBeUpdated);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project Result with ID:%s was not updated.", id);
                    }
                    return String.format("Project Result ID:%d to Project with title '%s' was updated successfully.",
                            updated.getId(), updated.getProject().getTitle());
                }),

                new Menu.Option("Delete Existing Project Result", () -> {
                    var id = new ProjectResultDialog().delete();
                    ProjectResult deleted;
                    try {
                        deleted = projectResultService.deleteById(loggedUser, id);
                    } catch (NonexistingEntityException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("Project Result ID:%d to Project with title '%s' was deleted successfully.",
                            deleted.getId(), deleted.getProject().getTitle());
                }),

                new Menu.Option("Print Single Project Result by ID", () -> {
                    var id = new ProjectResultDialog().printById();
                    ProjectResult projectResult;
                    try {
                        projectResult = projectResultService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another Project Result ID.";
                    }
                    System.out.println(projectResult);
                    return "";
                })

        ));
        menu.show();
    }


}
