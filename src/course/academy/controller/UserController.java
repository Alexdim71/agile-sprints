package course.academy.controller;

import course.academy.exception.NonexistingEntityException;
import course.academy.model.User;
import course.academy.service.UserService;
import course.academy.view.Menu;


import java.util.List;

import static course.academy.utils.ParsingHelpers.userInputNumber;

public class UserController {


    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public void init(User loggedUser) {
        var menu = new Menu("Users Menu", List.of(
                new Menu.Option("Print All Users", () -> {
                    var users = userService.getAll();
                    users.forEach(System.out::println);
                    return "Total user count: " + users.size();
                }),


                new Menu.Option("Print Single User by ID", () -> {
                    String messagePrintId = "Enter User ID: ";
                    var id = userInputNumber(messagePrintId);
                    User user;
                    try {
                        user = userService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another user id.";
                    }
                    System.out.println(user);
                    return "";
                })

        ));
        menu.show();
    }


}
