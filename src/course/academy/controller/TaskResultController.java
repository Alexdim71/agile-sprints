package course.academy.controller;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.TaskResult;
import course.academy.model.User;
import course.academy.model.mapper.TaskResultMapper;
import course.academy.service.TaskResultService;
import course.academy.view.Menu;
import course.academy.view.TaskResultDialog;

import java.util.List;

public class TaskResultController {

    private TaskResultService taskResultService;
    private TaskResultMapper taskResultMapper;

    public TaskResultController(TaskResultService taskResultService, TaskResultMapper taskResultMapper) {
        this.taskResultService = taskResultService;
        this.taskResultMapper = taskResultMapper;
    }

    public void init(User loggedUser) {
        var menu = new Menu("Task Results CRUD Menu", List.of(
                new Menu.Option("Print All Task Results", () -> {
                    var taskResults = taskResultService.getAll();
                    taskResults.forEach(System.out::println);
                    return "Total Task Results Count: " + taskResults.size();
                }),

                new Menu.Option("Enter New Task Result", () -> {
                    var dto = new TaskResultDialog().input();
                    TaskResult taskResult;
                    try {
                        taskResult = taskResultMapper.fromDto(dto);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Task Result was not added.");
                    }
                    TaskResult created;
                    try {
                        created = taskResultService.add(loggedUser, taskResult);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return String.format("Task Result to Task with ID:'%d' was not added.", dto.getTaskId());
                    }

                    return String.format("TaskResult ID:%d was added successfully.", created.getId());
                }),

                new Menu.Option("Update Existing Task Result", () -> {
                    var id = new TaskResultDialog().update();
                    TaskResult toBeUpdated;
                    try {
                        toBeUpdated = taskResultService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    var dto = new TaskResultDialog().input();
                    try {
                        toBeUpdated = taskResultMapper.fromDto(dto, id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("TaskResult with ID:'%d' was not updated.", id);
                    }
                    TaskResult updated;
                    try {
                        updated = taskResultService.update(loggedUser, toBeUpdated);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("TaskResult with ID:'%d' was not updated.", id);
                    }
                    return String.format("TaskResult with ID:%d was updated successfully.",
                            updated.getId());
                }),

                new Menu.Option("Delete Existing Task Result", () -> {
                    var id = new TaskResultDialog().delete();
                    TaskResult deleted;
                    try {
                        deleted = taskResultService.deleteById(loggedUser, id);
                    } catch (NonexistingEntityException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("TaskResult with ID:%d was deleted successfully.",
                            deleted.getId());
                }),

                new Menu.Option("Print Single Task Result by ID", () -> {
                    var id = new TaskResultDialog().printById();
                    TaskResult taskResult;
                    try {
                        taskResult = taskResultService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another Task Result ID.";
                    }
                    System.out.println(taskResult);
                    return "";
                })

        ));
        menu.show();
    }


}
