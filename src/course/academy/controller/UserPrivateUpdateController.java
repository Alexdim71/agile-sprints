package course.academy.controller;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.User;
import course.academy.model.dto.UserDto;
import course.academy.model.mapper.UserMapper;
import course.academy.service.UserService;
import course.academy.view.*;

import java.util.List;


public class UserPrivateUpdateController {

    private UserService userService;
    private UserMapper userMapper;

    public UserPrivateUpdateController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }


    public void init(User loggedUser) {
        var menu = new Menu("USER MENU", List.of(
                new Menu.Option("Update User Details", () -> {
                    UserDto dto = userMapper.userToDto(loggedUser);
                    var updatedDto = new UserPrivateUpdateDialog(dto).input();
                    User updatedUser = userMapper.fromDto(updatedDto, loggedUser.getId());


                    try {
                        userService.update(loggedUser, updatedUser);
                    } catch (NonexistingEntityException | InvalidEntityDataException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return String.format("User with username '%s' was not updated.", loggedUser.getUsername());
                    }

                    return String.format("User ID:%d with username '%s' updated successfully.",
                            loggedUser.getId(), loggedUser.getUsername());
                }),

                new Menu.Option("Change Password", () -> {

                    var oldPassword = new ChangePasswordDialog().inputOldPassword();
                    if (!loggedUser.getPassword().equals(oldPassword)) {
                        return "Error: wrong password";
                    } else {
                        var newPassword = new ChangePasswordDialog().inputNewPassword();
                        var repeatNewPassword = new ChangePasswordDialog().repeatNewPassword();
                        if (!newPassword.equals(repeatNewPassword)) {
                            return "Error: new password does not match.";
                        }
                        try {
                            userService.changePassword(loggedUser, loggedUser, newPassword);
                        } catch (UnauthorisedOperationException | InvalidEntityDataException e) {
                            System.out.println(e.getMessage());
                        }
                        return String.format("Password of user ID:%d with username %s changed successfully.",
                                loggedUser.getId(), loggedUser.getUsername());
                    }
                })

        ));
        menu.show();
    }


}
