package course.academy.controller;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.SprintResult;
import course.academy.model.User;
import course.academy.model.mapper.SprintResultMapper;
import course.academy.service.SprintResultService;
import course.academy.view.Menu;
import course.academy.view.SprintResultDialog;

import java.util.List;

public class SprintResultController {

    private SprintResultService sprintResultService;
    private SprintResultMapper sprintResultMapper;
    private User loggedUser;

    public SprintResultController(SprintResultService sprintResultService, SprintResultMapper sprintResultMapper) {
        this.sprintResultService = sprintResultService;
        this.sprintResultMapper = sprintResultMapper;
    }

    public void init(User loggedUser) {
        var menu = new Menu("Sprint Results CRUD Menu", List.of(
                new Menu.Option("Print All Sprint Results", () -> {
                    var sprintResults = sprintResultService.getAll();
                    sprintResults.forEach(System.out::println);
                    return "Total sprint count: " + sprintResults.size();
                }),

                new Menu.Option("Enter New Sprint Result", () -> {
                    var dto = new SprintResultDialog().input();
                    SprintResult sprintResult;
                    try {
                        sprintResult = sprintResultMapper.fromDto(dto);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("SprintResult was not added.");
                    }
                    SprintResult created;
                    try {
                        created = sprintResultService.add(loggedUser, sprintResult);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return String.format("Sprint Result to Sprint with ID:%d was not added.", dto.getSprintId());
                    }

                    return String.format("Sprint Result ID:%d to Sprint with ID:%d was added successfully.",
                            created.getId(), created.getSprint().getId());
                }),

                new Menu.Option("Update Existing Sprint Result", () -> {
                    var id = new SprintResultDialog().update();
                    SprintResult toBeUpdated;
                    try {
                        toBeUpdated = sprintResultService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    var dto = new SprintResultDialog().input();
                    try {
                        toBeUpdated = sprintResultMapper.fromDto(dto, id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Sprint Result with ID:%d was not updated.", id);
                    }
                    SprintResult updated;
                    try {
                        updated = sprintResultService.update(loggedUser, toBeUpdated);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Sprint Result with ID:%d was not updated.", id);
                    }
                    return String.format("Sprint Result ID:%d to Sprint with ID:'%d'; was updated successfully.",
                            updated.getId(), updated.getSprint().getId());
                }),

                new Menu.Option("Delete Existing Sprint Result", () -> {
                    var id = new SprintResultDialog().delete();
                    SprintResult deleted;
                    try {
                        deleted = sprintResultService.deleteById(loggedUser, id);
                    } catch (NonexistingEntityException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("Sprint Result ID:%d to Sprint with ID:'%d' was deleted successfully.",
                            deleted.getId(), deleted.getSprint().getId());
                }),

                new Menu.Option("Print Single Sprint Result by ID", () -> {
                    var id = new SprintResultDialog().printById();
                    SprintResult sprintResult;
                    try {
                        sprintResult = sprintResultService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another Sprint Result ID.";
                    }
                    System.out.println(sprintResult);
                    return "";
                })

        ));
        menu.show();
    }


}
