package course.academy.controller;

import course.academy.exception.InvalidEntityDataException;
import course.academy.exception.NonexistingEntityException;
import course.academy.exception.UnauthorisedOperationException;
import course.academy.model.Project;
import course.academy.model.Sprint;
import course.academy.model.User;
import course.academy.model.mapper.ProjectMapper;
import course.academy.service.ProjectService;
import course.academy.service.SprintService;
import course.academy.service.UserService;
import course.academy.view.Menu;
import course.academy.view.ProjectDialog;

import java.util.List;

public class ProjectController {

    private ProjectService projectService;
    private ProjectMapper projectMapper;
    private UserService userService;
    private SprintService sprintService;

    public ProjectController(ProjectService projectService, ProjectMapper projectMapper, UserService userService,
                             SprintService sprintService) {
        this.projectService = projectService;
        this.projectMapper = projectMapper;
        this.userService = userService;
        this.sprintService = sprintService;
    }

    public void init(User loggedUser) {
        var menu = new Menu("Projects CRUD Menu", List.of(
                new Menu.Option("Print All Projects", () -> {
                    var projects = projectService.getAll();
                    projects.forEach(System.out::println);
                    return "Total project count: " + projects.size();
                }),

                new Menu.Option("Enter New Project", () -> {
                    var dto = new ProjectDialog().input();
                    Project project;
                    try {
                        project = projectMapper.fromDto(dto);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project with title '%s' was not added.", dto.getTitle());
                    }
                    Project created;
                    try {
                        created = projectService.add(loggedUser, project);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project with title '%s' was not added.", dto.getTitle());
                    }

                    return String.format("Project ID:%d with title '%s' was added successfully.",
                            created.getId(), created.getTitle());
                }),

                new Menu.Option("Update Existing Project", () -> {
                    var id = new ProjectDialog().update();
                    Project toBeUpdated;
                    try {
                        toBeUpdated = projectService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    var dto = new ProjectDialog().input();
                    try {
                        toBeUpdated = projectMapper.fromDto(dto, id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project with title '%s' was not updated.", dto.getTitle());
                    }
                    Project updated;
                    try {
                        updated = projectService.update(loggedUser, toBeUpdated);
                    } catch (InvalidEntityDataException | UnauthorisedOperationException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return String.format("Project with title '%s' was not updated.", dto.getTitle());
                    }
                    return String.format("Project ID:%d with title '%s' was updated successfully.",
                            updated.getId(), updated.getTitle());
                }),

                new Menu.Option("Delete Existing Project", () -> {
                    var id = new ProjectDialog().delete();
                    Project deleted;
                    try {
                        deleted = projectService.deleteById(loggedUser, id);
                    } catch (NonexistingEntityException | UnauthorisedOperationException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("Project ID:%d with title '%s' was deleted successfully.",
                            deleted.getId(), deleted.getTitle());
                }),

                new Menu.Option("Print Single Project by ID", () -> {
                    var id = new ProjectDialog().printById();
                    Project project;
                    try {
                        project = projectService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another project id.";
                    }
                    System.out.println(project);
                    return "";
                }),

                new Menu.Option("Add Developer to Project", () -> {
                    var id = new ProjectDialog().printById();
                    Project project;
                    try {
                        project = projectService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another project id.";
                    }
                    var userId = new ProjectDialog().addDeveloperMessage();
                    User user;
                    try {
                        user = userService.getById(userId);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another user id.";
                    }
                    try {
                        projectService.addDeveloperToProject(loggedUser,project,user);
                    } catch (UnauthorisedOperationException | InvalidEntityDataException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }

                    return String.format("User with username '%s' was added to Project ID:%d",
                            user.getUsername(), project.getId());
                }),

                new Menu.Option("Remove Developer from Project", () -> {
                    var id = new ProjectDialog().printById();
                    Project project;
                    try {
                        project = projectService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another project id.";
                    }
                    var userId = new ProjectDialog().removeDeveloperMessage();
                    User user;
                    try {
                        user = userService.getById(userId);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another user id.";
                    }
                    try {
                        projectService.removeDeveloperFromProject(loggedUser,project,user);
                    } catch (UnauthorisedOperationException | InvalidEntityDataException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }

                    return String.format("User with username '%s' was removed from Project ID:%d",
                            user.getUsername(), project.getId());
                }),

                new Menu.Option("Set Current Sprint", () -> {
                    var id = new ProjectDialog().printById();
                    Project project;
                    try {
                        project = projectService.getById(id);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another project id.";
                    }
                    var sprintId = new ProjectDialog().setSprintMessage();
                    Sprint sprint;
                    try {
                        sprint = sprintService.getById(sprintId);
                    } catch (NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Try another sprint id.";
                    }
                    try {
                        projectService.setCurrentSprint(loggedUser, project, sprint);
                    } catch (UnauthorisedOperationException | InvalidEntityDataException | NonexistingEntityException e) {
                        System.out.println(e.getMessage());
                        return "Operation was not executed.";
                    }
                    return String.format("Sprint with ID:%d was set as the current sprint to Project ID:%d",
                            sprint.getId(), project.getId());
                }),

                new Menu.Option("Search Projects", () -> {
                    var searchString = new ProjectDialog().inputSearchString();
                    var projects = projectService.searchProject(searchString);
                    projects.forEach(System.out::println);
                    return "Total project count: " + projects.size();
                })


        ));

        menu.show();
    }


}
