package course.academy.view;


import course.academy.model.Kind;
import course.academy.model.StatusTask;
import course.academy.model.StatusUser;
import course.academy.model.Task;
import course.academy.model.dto.TaskDto;

import static course.academy.model.Task.TITLE_MAX_LENGTH;
import static course.academy.model.Task.TITLE_MIN_LENGTH;

import java.util.Scanner;

import static course.academy.utils.ParsingHelpers.userInputNumber;

public class TaskDialog implements EntityDialog<TaskDto> {
    public static Scanner sc = new Scanner(System.in);

    @Override
    public TaskDto input() {
        var dto = new TaskDto();

        String messageProject = "Enter Project ID: ";
        long idProject = userInputNumber(messageProject);
        dto.setProjectId(idProject);

        while (dto.getKind() == null) {
            System.out.println("Enter Kind (RESEARCH, DESIGN, PROTOTYPING, IMPLEMENTATION, QA, OPERATIONS, BUG_FIXING, DOCUMENTATION, OTHER): ");
            String ans = sc.nextLine();
            Kind kind = null;
            try {
                kind = Kind.valueOf(ans.toUpperCase());
            } catch (IllegalArgumentException e) {
                System.out.println("Please enter correct type of kind");
            }
            dto.setKind(kind);
        }

        String messageEffort = "Enter Estimated Effort: ";
        int effort = (int) userInputNumber(messageEffort);
        dto.setEstimatedEffort(effort);

        while (dto.getTitle() == null) {
            System.out.println("Task title: ");
            var ans = sc.nextLine();
            int length = ans.length();
            if (length < TITLE_MIN_LENGTH || length > TITLE_MAX_LENGTH) {
                System.out.println((String.format("Task title must be with length between %d and %d characters",
                        TITLE_MIN_LENGTH, TITLE_MAX_LENGTH)));
            } else {
                dto.setTitle(ans);
            }
        }

        while (dto.getTags() == null) {
            System.out.println("Tags, separate by ',': ");
            var ans = sc.nextLine();
            dto.setTags(ans);
        }

        String ans;
        while (dto.getDescription() == null) {
            System.out.println("Enter description(optional, press 'enter' to skip): ");
            ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            } else {
                if (ans.length() < Task.DESCRIPTION_MIN_LENGTH || ans.length() > Task.DESCRIPTION_MAX_LENGTH) {
                    System.out.println((String.format("Project description must be between %d and %d characters.",
                            Task.DESCRIPTION_MIN_LENGTH, Task.DESCRIPTION_MAX_LENGTH)));
                } else {
                    dto.setDescription(ans);
                }
            }
        }
        return dto;
    }

    public long update() {
        String messageUpdate = "Enter Task ID to be updated: ";
        return userInputNumber(messageUpdate);
    }

    public long delete() {
        String messageDelete = "Enter Task ID to be deleted: ";
        return userInputNumber(messageDelete);
    }

    public long printById() {
        String messagePrintId = "Enter Task ID: ";
        return userInputNumber(messagePrintId);
    }

    public StatusTask enterStatusTask() {
        int ans = 0;
        StatusTask status = null;
        while (ans == 0) {
            System.out.println("Update Task Status (PLANNED, ACTIVE, COMPLETED)");
            String input = sc.nextLine();
            try {
                status = StatusTask.valueOf(input.toUpperCase());
            } catch (IllegalArgumentException e) {
                System.out.println("Please enter correct type of User Status");
                continue;
            }
            ans = 1;
        }
        return status;
    }

    public String inputSearchString() {
        System.out.println("Task search word (if empty, all tasks are printed): ");
        return sc.nextLine();
    }

    }


