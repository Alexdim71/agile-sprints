package course.academy.view;

import course.academy.model.dto.SprintDto;
import course.academy.utils.ParsingHelpers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static course.academy.utils.ParsingHelpers.userInputNumber;

public class SprintDialog implements EntityDialog<SprintDto> {
    public static Scanner sc = new Scanner(System.in);

    @Override
    public SprintDto input() {
        var dto = new SprintDto();

        String messageProject = "Enter Project ID: ";
        long idProject = userInputNumber(messageProject);
        dto.setProjectId(idProject);

        while (dto.getStartDate() == null) {
            System.out.println("Start date in format dd.mm.yyyy: ");
            String ans;
            ans = sc.nextLine();
            LocalDate startDate = null;
            try {
                startDate = LocalDate.parse(ans, ParsingHelpers.df);
            } catch (Exception e) {
                System.out.println("Error: Invalid date - try again in the specified format.");
            }
            dto.setStartDate(startDate);
        }

        String messageDuration = "Enter duration";
        int duration = (int) userInputNumber(messageDuration);
        dto.setDuration(duration);

        String ans;
        while (dto.getDevelopersId() == null) {
            System.out.println("Enter developer IDs (positive integers), separated by ',' (optional, press 'enter' to skip and add later): ");
            ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            List<Integer> developers = new ArrayList<>();
            var input = ans.split(",");
            int devInt = 0;
            try {
                for (String devString: input) {
                        devInt = Integer.parseInt(devString);
                        developers.add(devInt);
                    }
            } catch (NumberFormatException e) {
                System.out.println("Please enter only positive numbers");
                continue;
            }
            dto.setDevelopersId(developers);
        }
        return dto;
    }


    public long update() {
        String messageUpdate = "Enter Sprint ID to be updated: ";
        return userInputNumber(messageUpdate);
    }

    public long delete() {
        String messageDelete = "Enter Sprint ID to be deleted: ";
        return userInputNumber(messageDelete);
    }

    public long printById() {
        String messagePrintId = "Enter Sprint ID: ";
        return userInputNumber(messagePrintId);
    }

    public long enterSprint() {
        String messageRemoveDeveloper = "Enter Sprint ID: ";
        return userInputNumber(messageRemoveDeveloper);
    }

    public long addTaskMessage() {
        String messageAddTask = "Enter Task ID to be added: ";
        return userInputNumber(messageAddTask);
    }

    public long removeTaskMessage() {
        String messageRemoveTask = "Enter Task ID to be removed: ";
        return userInputNumber(messageRemoveTask);
    }

    public long addDeveloperMessage() {
        String messageAddDeveloper = "Enter Developer ID to be added: ";
        return userInputNumber(messageAddDeveloper);
    }

    public long removeDeveloperMessage() {
        String messageRemoveDeveloper = "Enter Developer ID to be removed: ";
        return userInputNumber(messageRemoveDeveloper);
    }

}


