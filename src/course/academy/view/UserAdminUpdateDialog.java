package course.academy.view;

import course.academy.model.Role;
import course.academy.model.StatusUser;
import course.academy.model.dto.UserManagementDto;

import java.util.*;

import static course.academy.model.User.*;
import static course.academy.utils.ParsingHelpers.userInputNumber;

public class UserAdminUpdateDialog implements EntityDialog<UserManagementDto> {
    public static Scanner sc = new Scanner(System.in);

    private UserManagementDto dto;

    public UserAdminUpdateDialog() {
    }

    public UserAdminUpdateDialog(UserManagementDto dto) {
        this.dto = dto;
    }

    public long update() {
        String messageUpdate = "Enter User ID to be updated: ";
        return userInputNumber(messageUpdate);
    }

    @Override
    public UserManagementDto input() {
        int ans1 = 0;
        while (ans1 == 0) {
            System.out.println(String.format("Update first name '%s', press 'enter' to skip): ", dto.getFirstName()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            if (ans.length() < FIRST_NAME_MIN_LENGTH || ans.length() > FIRST_NAME_MAX_LENGTH) {
                System.out.println((String.format("First name must be with length between %d and %d characters",
                        FIRST_NAME_MIN_LENGTH, FIRST_NAME_MAX_LENGTH)));
            } else {
                dto.setFirstName(ans);
                ans1 = 1;
            }
        }

        int ans2 = 0;
        while (ans2 == 0) {
            System.out.println(String.format("Update last name '%s', press 'enter' to skip): ", dto.getLastName()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            if (ans.length() < LAST_NAME_MIN_LENGTH || ans.length() > LAST_NAME_MAX_LENGTH) {
                System.out.println((String.format("Last name must be with length between %d and %d characters",
                        LAST_NAME_MIN_LENGTH, LAST_NAME_MAX_LENGTH)));
            } else {
                dto.setLastName(ans);
                ans2 = 1;
            }
        }

        int ans3 = 0;
        while (ans3 == 0) {
            System.out.println(String.format("Update email '%s', press 'enter' to skip): ", dto.getEmail()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            if (!ans.matches(checkEmail)) {
                System.out.println("This is not a valid mail address.");
            } else {
                dto.setEmail(ans);
                ans3 = 1;
            }
        }

        int ans4 = 0;
        while (ans4 == 0) {
            System.out.println(String.format("Update contacts '%s', press 'enter' to skip): ", dto.getContacts()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            if (ans.length() < CONTACTS_MIN_LENGTH || ans.length() > CONTACTS_MAX_LENGTH) {
                System.out.println((String.format("Field contact must be with length between %d and %d characters",
                        CONTACTS_MIN_LENGTH, CONTACTS_MAX_LENGTH)));
            } else {
                dto.setLastName(ans);
                ans4 = 1;
            }
        }

        int ans5 = 0;
        while (ans5 == 0) {
            System.out.println(String.format("Update User Status '%s' (ACTIVE, CHANGE_PASSWORD, SUSPENDED, DEACTIVATED), press 'enter' to skip): ",
                    dto.getStatusUser()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            } else {
                StatusUser status = null;
                try {
                    status = StatusUser.valueOf(ans.toUpperCase());
                } catch (IllegalArgumentException e) {
                    System.out.println("Please enter correct type of User Status");
                    continue;
                }
                dto.setStatusUser(status);
                ans5 = 1;
            }
        }

        int ans6 = 0;
        while (ans6 == 0) {
            System.out.println(String.format("Update User Roles %s (DEVELOPER, PRODUCT_OWNER, ADMIN - please enter one or more, separated by ','), press 'enter' to skip): ",
                    dto.getRoles().toString()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            Set<Role> roles = new HashSet<>();
            var input = ans.split(",");
            Role role;
            try {
                for (String roleString : input) {
                    role = Role.valueOf(roleString.toUpperCase());
                    roles.add(role);
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Please enter correct type of roles");
                continue;
            }
            dto.setRoles(roles);
            ans6 = 1;
        }
        return dto;
    }
}