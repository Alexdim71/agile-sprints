package course.academy.view;

import course.academy.model.dto.UserDto;
import java.util.Scanner;

import static course.academy.model.User.*;

public class UserPrivateUpdateDialog implements EntityDialog<UserDto> {
    public static Scanner sc = new Scanner(System.in);

    private UserDto dto;

    public UserPrivateUpdateDialog(UserDto dto) {
        this.dto = dto;
    }

    @Override
    public UserDto input() {
        int ans1 = 0;
        while (ans1 == 0) {
            System.out.println(String.format("Update first name '%s', press 'enter' to skip): ", dto.getFirstName()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            if (ans.length() < FIRST_NAME_MIN_LENGTH || ans.length() > FIRST_NAME_MAX_LENGTH) {
                System.out.println((String.format("First name must be with length between %d and %d characters",
                        FIRST_NAME_MIN_LENGTH, FIRST_NAME_MAX_LENGTH)));
            } else {
                dto.setFirstName(ans);
                ans1 = 1;
            }
        }

        int ans2 = 0;
        while (ans2 == 0) {
            System.out.println(String.format("Update last name '%s', press 'enter' to skip): ", dto.getLastName()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            if (ans.length() < LAST_NAME_MIN_LENGTH || ans.length() > LAST_NAME_MAX_LENGTH) {
                System.out.println((String.format("Last name must be with length between %d and %d characters",
                        LAST_NAME_MIN_LENGTH, LAST_NAME_MAX_LENGTH)));
            } else {
                dto.setLastName(ans);
                ans2=1;
            }
        }

        int ans3 = 0;
        while (ans3 == 0) {
            System.out.println(String.format("Update email '%s', press 'enter' to skip): ", dto.getEmail()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            if (!ans.matches(checkEmail)) {
                System.out.println("This is not a valid mail address.");
            } else {
                dto.setEmail(ans);
                ans3=1;
            }
        }

        int ans4 = 0;
        while (ans4 == 0) {
            System.out.println(String.format("Update contacts '%s', press 'enter' to skip): ", dto.getContacts()));
            String ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            if (ans.length() < CONTACTS_MIN_LENGTH || ans.length() > CONTACTS_MAX_LENGTH) {
                System.out.println((String.format("Field contact must be with length between %d and %d characters",
                        CONTACTS_MIN_LENGTH, CONTACTS_MAX_LENGTH)));
            } else {
                dto.setLastName(ans);
                ans4 = 1;
            }
        }
        return dto;
    }



}