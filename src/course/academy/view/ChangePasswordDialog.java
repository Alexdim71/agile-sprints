package course.academy.view;

import java.util.Scanner;

import static course.academy.model.User.*;
import static course.academy.utils.MessageHelpers.INVALID_PASSWORD;

public class ChangePasswordDialog {
    public static Scanner sc = new Scanner(System.in);

    public String inputOldPassword() {
        String oldPassword = null;;
        while (oldPassword == null) {
            System.out.println("Enter old password: ");
            var ans = sc.nextLine();
            if (!ans.matches(checkPassword)) {
                System.out.println((String.format(INVALID_PASSWORD, USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH)));
            } else {
                oldPassword = ans;
            }
        }
        return oldPassword;
    }

    public String inputNewPassword() {
        String newPassword = null;
        while (newPassword == null) {
            System.out.println("New Password: ");
            var ans = sc.nextLine();
            if (!ans.matches(checkPassword)) {
                System.out.println((String.format(INVALID_PASSWORD, USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH)));
            } else {
                newPassword = ans;
            }
        }
        return newPassword;
    }

    public String repeatNewPassword() {
        String newPassword = null;
        while (newPassword == null) {
            System.out.println("Repeat New Password: ");
            newPassword = sc.nextLine();
        }
        return newPassword;
    }


}

