package course.academy.view;

import course.academy.model.User;

import java.util.Scanner;

import static course.academy.model.User.*;
import static course.academy.utils.MessageHelpers.INVALID_PASSWORD;

public class UserRegisterDialog implements EntityDialog<User> {
    public static Scanner sc = new Scanner(System.in);

    @Override
    public User input() {
        var user = new User();

        while (user.getFirstName() == null) {
            System.out.println("First name: ");
            var ans = sc.nextLine();
            int firstNameLength = ans.length();
            if (firstNameLength < FIRST_NAME_MIN_LENGTH || firstNameLength > FIRST_NAME_MAX_LENGTH) {
                System.out.println((String.format("First name must be with length between %d and %d characters",
                        FIRST_NAME_MIN_LENGTH, FIRST_NAME_MAX_LENGTH)));
            } else {
                user.setFirstName(ans);
            }
        }

        while (user.getLastName() == null) {
            System.out.println("Last name: ");
            var ans = sc.nextLine();
            int lastNameLength = ans.length();
            if (lastNameLength < LAST_NAME_MIN_LENGTH || lastNameLength > LAST_NAME_MAX_LENGTH) {
                System.out.println((String.format("Last name must be with length between %d and %d characters",
                        LAST_NAME_MIN_LENGTH, LAST_NAME_MAX_LENGTH)));
            } else {
                user.setLastName(ans);
            }
        }

        while (user.getEmail() == null) {
            System.out.println("Email: ");
            var ans = sc.nextLine();
            if (!ans.matches(checkEmail)) {
                System.out.println("This is not a valid mail address.");
            } else {
                user.setEmail(ans);
            }
        }

        while (user.getUsername() == null) {
            System.out.println("Username: ");
            var ans = sc.nextLine();
            if (!ans.matches(checkUsername)) {
                System.out.println((String.format("Username must be between %d and %d characters and contain only word characters",
                        USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH)));
            } else {
                user.setUsername(ans);
            }
        }

        while (user.getPassword() == null) {
            System.out.println("Password: ");
            var ans = sc.nextLine();
            if (!ans.matches(checkPassword)) {
                System.out.println((String.format(INVALID_PASSWORD, USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH)));
            } else {
              user.setPassword(ans);
            }
        }

        return user;
    }

}

