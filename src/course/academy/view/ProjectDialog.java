package course.academy.view;


import course.academy.model.dto.ProjectDto;
import course.academy.utils.ParsingHelpers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static course.academy.model.Project.*;
import static course.academy.model.Project.DESCRIPTION_MAX_LENGTH;
import static course.academy.model.User.*;
import static course.academy.utils.ParsingHelpers.userInputNumber;

public class ProjectDialog implements EntityDialog<ProjectDto> {
    public static Scanner sc = new Scanner(System.in);

    @Override
    public ProjectDto input() {
        var dto = new ProjectDto();

        while (dto.getTitle() == null) {
            System.out.println("Project title: ");
            var ans = sc.nextLine();
            int length = ans.length();
            if (length < TITLE_MIN_LENGTH || length > TITLE_MAX_LENGTH) {
                System.out.println((String.format("Project title must be with length between %d and %d characters",
                        TITLE_MIN_LENGTH, TITLE_MAX_LENGTH)));
            } else {
                dto.setTitle(ans);
            }
        }

        while (dto.getStartDate() == null) {
            System.out.println("Start date in format dd.mm.yyyy: ");
            String ans;
            ans = sc.nextLine();
            LocalDate startDate = null;
            try {
                startDate = LocalDate.parse(ans, ParsingHelpers.df);
            } catch (Exception e) {
                System.out.println("Error: Invalid date - try again in the specified format.");
            }
            dto.setStartDate(startDate);
        }

        while (dto.getOwnerId() == 0) {
            System.out.println("Owner ID: ");
            long ownerId = 0;
            try {
                ownerId = Integer.parseInt(sc.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Error: please enter a positive integer value.");
            }
            dto.setOwnerId(ownerId);
        }

        while (dto.getTags() == null) {
            System.out.println("Tags, separate by ',': ");
            var ans = sc.nextLine();
            dto.setTags(ans);
        }

        String ans;
        while (dto.getDescription() == null) {
            System.out.println("Enter description(optional, press 'enter' to skip): ");
            ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            } else {
                if (ans.length() < DESCRIPTION_MIN_LENGTH || ans.length() > DESCRIPTION_MAX_LENGTH) {
                    System.out.println((String.format("Project description must be between %d and %d characters.",
                            DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH)));
                } else {
                    dto.setDescription(ans);
                }
            }
        }

        while (dto.getDeveloperIds() == null) {
            System.out.println("Enter developer IDs (positive integers), separated by ',' (optional, press 'enter' to skip and add later): ");
            ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            List<Integer> developers = new ArrayList<>();
            var input = ans.split(",");
            int devInt = 0;
            try {
                for (String devString: input) {
                    devInt = Integer.parseInt(devString);
                    developers.add(devInt);
                }
            } catch (NumberFormatException e) {
                System.out.println("Please enter only positive numbers");
                continue;
            }
            dto.setDeveloperIds(developers);
        }

        while (dto.getTasksIds() == null) {
            System.out.println("Enter task IDs (positive integer), separated by ',' (optional, press 'enter' to skip and add later): ");
            ans = sc.nextLine();
            if (ans.isEmpty()) {
                break;
            }
            List<Integer> tasks = new ArrayList<>();
            var input = ans.split(",");
            int taskInt = 0;
            try {
                for (String taskString: input) {
                    taskInt = Integer.parseInt(taskString);
                    tasks.add(taskInt);
                }
            } catch (NumberFormatException e) {
                System.out.println("Please enter only positive numbers");
                continue;
            }
            dto.setTasksIds(tasks);
        }

        return dto;
    }

    public long update() {
        String messageUpdate = "Enter Project ID to be updated: ";
        return userInputNumber(messageUpdate);
    }

    public long delete() {
        String messageDelete = "Enter Project ID to be deleted: ";
        return userInputNumber(messageDelete);
    }

    public long printById() {
        String messagePrintId = "Enter Project ID: ";
        return userInputNumber(messagePrintId);
    }

    public long addDeveloperMessage() {
        String messageAddDeveloper = "Enter Developer ID to be added: ";
        return userInputNumber(messageAddDeveloper);
    }

    public long removeDeveloperMessage() {
        String messageRemoveDeveloper = "Enter Developer ID to be removed: ";
        return userInputNumber(messageRemoveDeveloper);
    }

    public long setSprintMessage() {
        String messageAddTask = "Enter Sprint ID to be set: ";
        return userInputNumber(messageAddTask);
    }

    public String inputSearchString() {
            System.out.println("Project search word (if empty, all projects are printed): ");
            return sc.nextLine();
    }

}


